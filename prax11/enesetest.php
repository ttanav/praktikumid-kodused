
<!DOCTYPE>
<html>
<header>
	<meta charset="utf-8" />
	<title>Enesetest 11</title>
	<script
src="http://code.jquery.com/jquery-1.11.0.min.js">
</script>
	<style type="text/css">
	fieldset {
		margin:5px;
	}
.correct {
	color:green;
}
.incorrect {
	color:red;
}
#action {
	position: fixed;
	left:0px;
	right: 0px;
	bottom: 0px;
	padding:10px 100px;
	background: #800080;
	border-top:solid grey 1px;
}
body {
	padding-bottom:50px;
}
input[type="radio"] {
	margin-top: 10px;
}
legend {font-weight: bold;}
	</style>
</header>
<body>
<?php // &lt;&gt;  &#36;

$test=array(
	array("q"=>"Tabeli 'loomaaed' <pre>+-------+-------------+------+-----+---------+----------------+
| Field | Type        | Null | Key | Default | Extra          |
+-------+-------------+------+-----+---------+----------------+
| id    | int(11)     | NO   | PRI | NULL    | auto_increment |
| nimi  | varchar(50) | NO   |     | NULL    |                |
| vanus | int(11)     | NO   |     | NULL    |                |
| liik  | varchar(50) | NO   |     | NULL    |                |
| puur  | int(11)     | NO   |     | NULL    |                |
+-------+-------------+------+-----+---------+----------------+
</pre> Loomiseks on kasutatud", "a"=>1, "o"=> array("CREATE loomaaed (id integer PRIMARY KEY, nimi varchar(50), vanus integer, liik varchar(50), puur integer)", "CREATE TABLE loomaaed (id integer PRIMARY KEY auto_increment, nimi varchar(50), vanus integer, liik varchar(50), puur integer)", "TABLE loomaaed (id integer, nimi varchar(50), vanus integer, liik varchar(50), puur integer)")),
	
	array("q"=>"primary key on", "a"=>0, "o"=> array("andmebaasitabeli välja omadus sisaldada konkreetset rida identifitseerivat väärtust, mis on tabeli piires ainulaadne", "andmebaasitabeli välja omadus sisaldada konkreetset rida identifitseerivat väärtust", "andmebaasitabeli välja omadus sisaldada tabeli piires ainulaadset väärtust")),
	
	array("q"=>"foreign key on", "a"=>1, "o"=> array("andmebaasitabeli välja omadus sisaldada väärtust, mis viitab mingi teise tabeli reale selle suvalise välja väärtuse põhjal", "andmebaasitabeli välja omadus sisaldada väärtust, mis viitab mingi teise tabeli reale tema primaarvõtme väärtuse põhjal", "andmebaasitabeli välja omadus sisaldada väärtust, mis viitab sama tabeli teisele reale tema primaarvõtme väärtuse põhjal")),
	
	array("q"=>"auto_increment on", "a"=>2, "o"=> array("andmebaasitabeli välja omadus, mille puhul suurendatakse tabelirea loomisel väljale määratud väärtust automaatselt ühe võrra", "andmebaasitabeli välja omadus, mille puhul tabelirea muutmisel suurendatakse välja väärtust automaatselt ühe võrra", "andmebaasitabeli välja omadus, mille puhul  määratakse rea loomisel välja väärtus andmebaasimootori poolt automaatselt nii, et ta on eelnevast ühe võrra suurem")),
	
	array("q"=>"väljale vaikimisi väärtuse lisamiseks", "a"=>0, "o"=> array("kirjutatakse tabeli loomisel selle välja tüübi järgi märksõna DEFAULT ning vaikimisi väärtus (nt. vanus integer DEFAULT 0)", "kirjutatakse tabeli loomisel selle välja tüübi järgi vaikimisi väärtus (nt. vanus integer 0)", "kirjutatakse tabeli loomisel selle välja tüübi järgi märksõna DEFAULT ning vaikimisi väärtus (nt. vanus integer DEFAULT = 0)")),
	
	array("q"=>"tabeli kustutamiseks käivitatakse", "a"=>1, "o"=> array("DELETE TABLE loomaaed; päring", "DROP TABLE loomaaed; päring", "DROP loomaaed; päring")),
	
	array("q"=>"Tulemus (lühendatud)<pre>+--------+-----+
| name   | age |
+--------+-----+
| Kaarel |   5 |
| Klaara |   3 |
...
| Taibu  |   6 |
+--------+-----+
10 rows in set (0.00 sec)</pre>on saadud päringuga", "a"=>0, "o"=> array("SELECT nimi as name, vanus as age FROM loomaaed;", "SELECT nimi, vanus FROM loomaaed;", "SELECT nimi as name, vanus as age;")),
	
	array("q"=>"WHERE klausel", "a"=>1, "o"=> array("piirab ainult SELECT päringutes hangitavate ridade hulka vastavalt nende väljade väärtustele", "piirab erinevates SQL päringutes, sh SELECT-is, hangitavate ridade hulka vastavalt nende väljade väärtustele")),
	
	array("q"=>"Tulemus <pre>+----+-------+-------+------+------+
| id | nimi   | vanus | liik | puur |
+----+--------+-------+------+------+
|  8 | Leo    |     2 | part |    5 |
|  9 | Donald |     8 | part |    5 |
+----+--------+-------+------+------+
2 rows in set (0.01 sec)</pre>on saadud päringuga", "a"=>2, "o"=> array("SELECT * FROM loomaaed WHERE puur=5;", "SELECT * FROM loomaaed WHERE liik=\"part\";", "ükskõik kumma ülevalmainituga")),
	
	array("q"=>"Tabelisse rea lisamiseks kasutatakse", "a"=>0, "o"=> array("INSERT INTO loomaaed (nimi, vanus, liik, puur) VALUES (\"Jaakob\", 2, \"jaaguar\", 9); päringut", "NEW ROW loomaaed (nimi, vanus, liik, puur) VALUES (\"Jaakob\", 2, \"jaaguar\", 9); päringut", "INSERT INTO loomaaed  VALUES (\"Jaakob\", 2, \"jaaguar\", 9); päringut")),
	
	array("q"=>"mitme rea samaaegseks sisestamiseks", "a"=>3, "o"=> array("käivitada mitu järjestikku insert käsku", "eraldada erinevate ridade väärtuseid sisaldavad sulud komadega (nt. ... VALUES ( 2014, 'sept'), (2014, 'okt'))", "kirjeldada komadega eraldatud sulgudes väljade väärtuseid erinevates ridades (nt. ... VALUES (2014, 2014), ('sept', 'okt'))", "sobivad esimesed kaks meetodit")),
	
	array("q"=>"tabelis olemasolevate ridade muutmiseks kasutatakse", "a"=>1, "o"=> array("SET puur=1 IN loomaaed; päringut", "UPDATE loomaaed SET puur=1; päringut", "UPDATE loomaaed puur=1; päringut")),
	
	array("q"=>"Tabelist ridade kustutamiseks kasutatakse", "a"=>0, "o"=> array("DELETE FROM loomaaed; päringut", "DELETE loomaaed; päringut", "DELETE * FROM loomaaed; päringut")),
	
	array("q"=>"pärast tabelist ridade kustutamist ridu sisestades", "a"=>1, "o"=> array("võetakse kõigepealt kustutatud ridade primaarvõtmete väärtused uuesti kasutusse ning seejärel genereeritakse uued võtmete väärtused suurima primaarvõtme järgi", "jätkatakse primaarvõtme väärtuse genereerimist sealt, kus ta eelmise sisestamise juures pooleli jäi"))/*,
	
	array("q"=>"", "a"=>1, "o"=> array("", "", "")),
	
	array("q"=>"", "a"=>1, "o"=> array("", "", "")),
	
	array("q"=>"", "a"=>1, "o"=> array("", "", "")),
	
	array("q"=>"", "a"=>1, "o"=> array("", "", ""))*/

	);
?>
<p>Enesetest on mõeldud selleks, et saaksite kontrollida oma arusaama loengus räägitud teemadel. Tulemusi ei salvestata.<br/>Küsimuste arv: <?php echo count($test); ?></p>
<form action="?" method="POST" id="enesele">
	<?php
		foreach($test as $id => $question){
			echo "<fieldset><legend>{$question['q']}</legend>";
			foreach ($question["o"] as $o_id => $o_text ){
				$extra="";
				$checked="";
				if (isset($_POST['answers']) && isset($_POST['answers'][$id]) && $_POST['answers'][$id]==$o_id){
					$checked="checked";
					if ($question['a']==$o_id) $extra="class=\"correct\"";
					else $extra="class=\"incorrect\"";
				}
				echo "<input type=\"radio\" id=\"a{$id}-{$o_id}\" name=\"answers[$id]\" value=\"{$o_id}\" {$checked} /><label for=\"a{$id}-{$o_id}\" {$extra} >{$o_text}</label><br/>";
			}
			echo "</fieldset>";
		}
	?><br/>

</form>
<p>On ideid rohkemateks küsimusteks? Kirjutage julgelt tiia.tanav[at]itcollege.ee </p>

<div id="action"><input type="submit" value="Testin teadmisi" form="enesele" /></div>
</body>
</html>