<?php


function connect_db(){
	global $connection;
	$host="localhost";
	$user="test";
	$pass="t3st3r123";
	$db="test";
	$connection = mysqli_connect($host, $user, $pass, $db) or die("ei saa ühendust mootoriga- ".mysqli_error());
	mysqli_query($connection, "SET CHARACTER SET UTF8") or die("Ei saanud baasi utf-8-sse - ".mysqli_error($connection));
}

function logi(){
	// siia on vaja funktsionaalsust
	global $connection;
	if (isset($_SESSION['user'])) {
		header("Location: ?page=loomad");
	}
	$errors=array();
	if (!empty($_POST)){

		if (empty($_POST['user'])){
			$errors[]="kasutajanimi puudu";
		} else {
			$user=mysqli_real_escape_string($connection, $_POST['user']);
		}
		if (empty($_POST['pass'])){
			$errors[]="parool puudu";
		} else {
			$pass=mysqli_real_escape_string($connection, $_POST['pass']);
		}
		if (empty($errors)){
			$r=mysqli_query($connection, "SELECT * from kylastajad WHERE username='$user' and passw=sha1('$pass')");
			if ($r){
				// logitud
				$_SESSION['user']=$user;
				header("Location: ?page=loomad");
				exit(0);
			}else {
				$errors[]="vale kasutajanimi/parool";
			}
		}
	}
	include_once('views/login.html');
}

function logout(){
	$_SESSION=array();
	session_destroy();
	header("Location: ?");
}

function kuva_puurid(){
	if (!isset($_SESSION['user'])) {
		header("Location: ?page=login");
	}
	// siia on vaja funktsionaalsust
	global $connection;
	$p= mysqli_query($connection, "select distinct(puur) as puur from loomaaed order by puur asc");
	$puurid=array();
	while ($r=mysqli_fetch_assoc($p)){
		$l=mysqli_query($connection, "SELECT * FROM loomaaed WHERE  puur=".mysqli_real_escape_string($connection, $r['puur']));
		while ($row=mysqli_fetch_assoc($l)) {
			$puurid[$r['puur']][]=$row;
		}
	}
	include_once('views/puurid.html');
	
}

function lisa(){
	// siia on vaja funktsionaalsust
	global $connection;
	if (!isset($_SESSION['user'])) {
		header("Location: ?page=login");
	}

	if (!empty($_POST)){
		$loom=array();
		if (empty($_POST['puur'])){
			$errors[]="puur puudu";
		} else {
			$loom['puur']=mysqli_real_escape_string($connection, $_POST['puur']);
		}
		if (empty($_POST['nimi'])){
			$errors[]="nimi puudu";
		} else {
			$loom['nimi']=mysqli_real_escape_string($connection, $_POST['nimi']);
		}
		if ($pilt=upload('liik')){
			$loom['liik']=$pilt;
		} else {
			$errors[]="pilt puudu";
		}
		if (empty($errors)){
			$r=mysqli_query($connection, "INSERT INTO loomaaed (nimi, liik, puur) values ('{$loom['nimi']}','{$loom['liik']}', {$loom['puur']} )");
			if ($r){
				header("Location: ?page=loomad");
				exit(0);
			}else {
				$errors[]="sisestamine ebaõnnestus";
			}
		}
	}

	include_once('views/loomavorm.html');
	
}

function upload($name){
	$allowedExts = array("jpg", "jpeg", "gif", "png");
	$allowedTypes = array("image/gif", "image/jpeg", "image/png","image/pjpeg");
	$extension = end(explode(".", $_FILES[$name]["name"]));

	if ( in_array($_FILES[$name]["type"], $allowedTypes)
		&& ($_FILES[$name]["size"] < 100000)
		&& in_array($extension, $allowedExts)) {
    // fail õiget tüüpi ja suurusega
		if ($_FILES[$name]["error"] > 0) {
			$_SESSION['notices'][]= "Return Code: " . $_FILES[$name]["error"];
			return "";
		} else {
      // vigu ei ole
			if (file_exists("pildid/" . $_FILES[$name]["name"])) {
        // fail olemas ära uuesti lae, tagasta failinimi
				$_SESSION['notices'][]= $_FILES[$name]["name"] . " juba eksisteerib. ";
				return "pildid/" .$_FILES[$name]["name"];
			} else {
        // kõik ok, aseta pilt
				move_uploaded_file($_FILES[$name]["tmp_name"], "pildid/" . $_FILES[$name]["name"]);
				return "pildid/" .$_FILES[$name]["name"];
			}
		}
	} else {
		return "";
	}
}

?>