
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Enesetest 13</title>
	<script src="http://code.jquery.com/jquery-1.11.0.min.js">
</script>
	<style type="text/css">
	fieldset {
		margin:5px;
	}
.correct {
	color:green;
}
.incorrect {
	color:red;
}
#action {
	position: fixed;
	left:0px;
	right: 0px;
	bottom: 0px;
	padding:10px 100px;
	background: #800080;
	border-top:solid grey 1px;
}
body {
	padding-bottom:50px;
}
input[type="radio"] {
	margin-top: 10px;
}
legend {font-weight: bold;}
	</style>
</head>
<body>
<?php // &lt;&gt;  &#36;

$test=array(
	array("q"=>"", "a"=>1, "o"=> array("", "", "")),
	
	array("q"=>"", "a"=>0, "o"=> array("", "", ""))

	);
?>
<p>Enesetest on mõeldud selleks, et saaksite kontrollida oma arusaama loengus räägitud teemadel. Tulemusi ei salvestata.<br/>Küsimuste arv: <?php echo count($test); ?></p>
<form action="?" method="POST" id="enesele">
	<?php
		foreach($test as $id => $question){
			echo "<fieldset><legend>{$question['q']}</legend>";
			foreach ($question["o"] as $o_id => $o_text ){
				$extra="";
				$checked="";
				if (isset($_POST['answers']) && isset($_POST['answers'][$id]) && $_POST['answers'][$id]==$o_id){
					$checked="checked";
					if ($question['a']==$o_id) $extra="class=\"correct\"";
					else $extra="class=\"incorrect\"";
				}
				echo "<input type=\"radio\" id=\"a{$id}-{$o_id}\" name=\"answers[$id]\" value=\"{$o_id}\" {$checked} /><label for=\"a{$id}-{$o_id}\" {$extra} >{$o_text}</label><br/>";
			}
			echo "</fieldset>";
		}
	?><br/>

</form>
<p>On ideid rohkemateks küsimusteks? Kirjutage julgelt tiia.tanav[at]itcollege.ee </p>

<div id="action"><input type="submit" value="Testin teadmisi" form="enesele" /></div>
</body>
</html>