<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Enesetest 04</title>
	<style type="text/css">
	fieldset {
		margin:5px;
	}
legend {font-weight: bold;}
.correct {
	color:green;
}
.incorrect {
	color:red;
}
#action {
	position: fixed;
	left:0px;
	right: 0px;
	bottom: 0px;
	padding:10px 100px;
	background: #800080;
	border-top:solid grey 1px;
}
body {
	padding-bottom:50px;
}
input[type="radio"] {
	margin-top: 10px;
}
	</style>
</head>
<body>
<?php // &lt;&gt;
$test=array(
	array("q"=>"padding on", "a"=>0, "o"=> array("ruum elemendi sisu ja piirjoone vahel", "ruum elemendi paddingu ja margini vahel", "vaba ruum elemendi ümber")),
	
	array("q"=>"margin on", "a"=>2, "o"=> array("ruum elemendi sisu ja piirjoone vahel", "ruum elemendi paddingu ja margini vahel", "vaba ruum elemendi ümber")),
	
	array("q"=>"inline tüüpi elementidel rakenduvad marginid-paddingud", "a"=>0, "o"=> array("ainult horisontaalsuunas", "ainult vertikaalsuunas", "kõigis suundades")),
	
	array("q"=>"elemendile on määratud stiil:<br/>min-width:600px; <br/>width:1000px; <br/>max-width:800px;<br/> Mis on selle elemendi laius ekraanil?", "a"=>2, "o"=> array("600px", "1000px", "800px")),
	
	array("q"=>"inline tüüpi elementide puhul", "a"=>3, "o"=> array("pole lootustki saada määrata elemendi suurust ning vertikaalseid margineid-paddinguid", "saab kuvamismeetodi kirjutada üle  display:block omadusega, mille tulemusena käitub ta nagu block tüüpi element", "saab kuvamismeetodi kirjutada üle  display:inline-block omadusega, mille tulemusena ta käitub nagu block tüüpi element, mille ees ja järel ei ole reavahetusi", "Kehtivad ülemistest väidetest vaid kaks viimast")),
	
	array("q"=>"kuvamismeetod (display väärtus) on", "a"=>5, "o"=> array("block - reavahetused enne ja pärast, marginid-paddingud igas suunas", "none - elelemti ei 'eksisteeri'", "inline - kuvatakse teksti osana, marginid-paddingud mõjuvad ainult horisontaalis", "initial - algne kuvamismeetod, sõltuvalt elemendi tüüpist", "inline-block - nagu block ilma reavahetusteta", "kõik eelnevad ja palju teisi")),
	
	array("q"=>"position: static ", "a"=>2, "o"=> array("pole reaalne positioneerimise meetod", "on elementide paigutamise meetod, mille puhul element püsib ekraanil staatiliselt ühes kohas paigal", "on elementide vaikimisi positsioneerimise meetod, mille puhul element asub seal, kus ta elementide ladumisjärjekorra tõtu olema peaks")),
	
	array("q"=>"position: relative", "a"=>1, "o"=> array("on elementide positsioneerimise meetod, mille puhul element paigutatakse teiste elementide suhtes", "on elementide positsioneerimise meetod, mille puhul element paigutatakse tema algse asukoha suhtes nii, et tema endine koht jääb tühjaks", "on elementide positsioneerimise meetod, mille puhul element paigutatakse tema algse asukoha suhtes nii, et tema endine koht täidetakse ladumisjärjekorras järgnevate elementidega")),
	
	array("q"=>"position: fixed", "a"=>2, "o"=> array("on elementide positsioneerimise meetod, mille puhul on positsioneerimine võrreldes eelnevate CSS versioonidega parandatud.", "on elementide positsioneerimise meetod, mille puhul element paigutatakse veebilehitseja akna suhtes nii, et element liigub lehe skrollimisel lehe muu sisuga kaasa", "on elementide positsioneerimise meetod, mille puhul element paigutatakse veebilehitseja akna suhtes nii, et element püsib lehe skrollimisel paigal")),
	
	array("q"=>"position: absolute", "a"=>0, "o"=> array("on elementide positsioneerimise meetod, mille puhul paigutatakse elementi tema mittestaatilise esivanema või selle puudumisel body suhtes", "on elementide positsioneerimise meetod, mille puhul paigutatakse elementi alati tema vanema külgede suhtes", "on elementide positsioneerimise meetod, mille puhul paigutatakse elementi alati body suhtes")),
	
	array("q"=>"position: left ja position:right", "a"=>1, "o"=> array("on positsioneerimise meetodid, mille puhul element paigutatakse vastavalt vasaku või parema ekraani või mittestaatilise vanemelemendi serva suhtes", "pole reaalsed positsioneerimise meetodid", "on positsioneerimise meetodid, mille puhul element paigutatakse vastavalt vasakusse või paremasse teda ümbritseva elemendi serva")),
	
	array("q"=>"position: float", "a"=>2, "o"=> array("on positsioneerimise meetod, mille puhul element hõljub teiste elementide vahel", "on positsioneerimise meetod, mille puhul element hõljub teidte elementide kohal", "pole reaalne positsioneerimise meetod")),
	
	array("q"=>"Positsioneerimismeetodid, mille puhul element muutub vaikimisi (kui pole laius määratud) nii kitsaks kui sisu võimaldab, on", "a"=>1, "o"=> array("kõik peale static ja absolute, mille puhul on element vanema laiune", "kõik peale static ja relative, mille puhul on element vanema laiune", "kõik peale absolute ja relative, mille puhul on element vanema laiune")),
	
	array("q"=>"Positsioneerimismeetodidi määramisega käivad käsikäes elemendi koordinaatide (asukoha) määramise omadused", "a"=>0, "o"=> array("top, left, bottom, right", "north, east, south, west", "x, y, z")),
	
	array("q"=>"float omadust kasutatakse", "a"=>2, "o"=> array("elementide sisu joonduse kas ühte või teise külge määramiseks", "elementide nende vanemate äärde paigutamiseks nii, et ülejäänud sisu paigutub nende taha", "elementide nende vanemate äärde paigutamiseks nii, et ülejäänud sisu paigutub nende ümber (kui mahub)")),
	
	array("q"=>"float omaduse väärtused on", "a"=>1, "o"=> array("top, bottom, none", "left, right, none", "top, left, right, bottom, none")),
	
	array("q"=>"samma suunda elementide hõljutamisel (float)", "a"=>2, "o"=> array("paiknevad elemendid vaikimisi üksteise alla", "paiknevad elemendid üksteise kõrvale nii, et dokumendis esimene element on hõljumisel alati vasakult lugedes esimene element", "paiknevad elemendid üksteise kõrvale nii, et dokumendis esimene element on hõljumisel alati vastavast suunast (paremalt / vasakult) lugedes esimene element")),
	
	array("q"=>"clear omadust kasutatakse", "a"=>0, "o"=> array("selleks, et element ei paikneks tema ees oleva hõljuvate elmementide kõrvale/vahele vaid asuks temast allpool", "selleks, et eemaldada elemendilt kõik rakendatud stiilireeglid", "selleks, et puhastada elemendi sisu")),
	
	array("q"=>"clear omaduse väärtused on", "a"=>1, "o"=> array("top, bottom, both, none", "left, right, both, none", "top, bottom, left, right, all, none")),
	
	array("q"=>"Kui väikses elemendis on liiga palju sisu (teksti, pilte), siis", "a"=>2, "o"=> array("lõigatakse elemendi sisu sealt maalt maha kust ta enam elemendi piiridesse ära ei mahu", "tekib automaatselt elemendile scrollbar, mille abiga saab näha kogu sisu väiksemal, piiritletud alal", "valgub sisu üle elemendi piiride")),
	
	array("q"=>"eelmises küsimuses tekkiva olukorra lahendamiseks", "a"=>3, "o"=> array("vähendada elemendis asuvat sisu nii, et sisu mahuks ära", "suurendada elemendi mõõtmeid mahutamaks ära tema sisu", "lisada elemendile omadus overflow, mis muudab elemendi skrollitavaks alaks", "sobivad kõik ülemised lahendused"))


	);
?>
<p>Enesetest on mõeldud selleks, et saaksite kontrollida oma arusaama loengus räägitud teemadel. Tulemusi ei salvestata.<br/>Küsimuste arv: <?php echo count($test); ?></p>
<form action="?" method="POST" id="enesele">
	<?php
		foreach($test as $id => $question){
			echo "<fieldset><legend>{$question['q']}</legend>";
			foreach ($question["o"] as $o_id => $o_text ){
				$extra="";
				$checked="";
				if (isset($_POST['answers']) && isset($_POST['answers'][$id]) && $_POST['answers'][$id]==$o_id){
					$checked="checked";
					if ($question['a']==$o_id) $extra="class=\"correct\"";
					else $extra="class=\"incorrect\"";
				}
				echo "<input type=\"radio\" id=\"a{$id}-{$o_id}\" name=\"answers[$id]\" value=\"{$o_id}\" {$checked} /><label for=\"a{$id}-{$o_id}\" {$extra} >{$o_text}</label><br/>";
			}
			echo "</fieldset>";
		}
	?><br/>

</form>
<p>On ideid rohkemateks küsimusteks? Kirjutage julgelt tiia.tanav[at]itcollege.ee </p>

<div id="action"><input type="submit" value="Testin teadmisi" form="enesele" /></div>
</body>
</html>
