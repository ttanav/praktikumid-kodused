<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Enesetest 06</title>
	<script
src="http://code.jquery.com/jquery-1.11.0.min.js">
</script>
	<style type="text/css">
	fieldset {
		margin:5px;
	}
.correct {
	color:green;
}
.incorrect {
	color:red;
}
#action {
	position: fixed;
	left:0px;
	right: 0px;
	bottom: 0px;
	padding:10px 100px;
	background: #800080;
	border-top:solid grey 1px;
}
body {
	padding-bottom:50px;
}
input[type="radio"] {
	margin-top: 10px;
}
legend {font-weight: bold;}
	</style>
</head>
<body>
<?php // &lt;&gt;
$test=array(
	array("q"=>"jQuery on", "a"=>1, "o"=> array("veebis kasutatav skriptimise keel, sarnane javaScriptiga", "erinevates veebilehitsejates samat moodi toimiva javaScripti kirjutamist võimaldav teek, mis lihtsustab elementide mõjutamist", "veebiarenduses kasutatav andmete baasist pärimise keel")),
	
	array("q"=>"jQuery kasutamiseks", "a"=>2, "o"=> array("pole vaja midagi erilist teha, lihtsalt kirjuta", "tuleb javascripti alguses teek importida", "tuleb tema teegifail html faili päises enne kõiki teisi script elemente kaasata")),
	
	array("q"=>"Kui jQueryt kirjutada eralsiseisvasse faili, siis on selle faili laiendiks", "a"=>0, "o"=> array(".js", ".jq", ".jqr")),
	
	array("q"=>"lehe laadimise lõpetamise sündmuse kirjeldamiseks on jQuerys", "a"=>2, "o"=> array("kirjapilt<pre>$(document).onready(function(){ /* valmis */ })</pre>", "kirjapilt<pre>$(document).ready = function(){ /* valmis */ }</pre>", "kirjapilt<pre>$(document).ready(function(){ /* valmis */ })</pre>")),
	
	array("q"=>"elementide selekteerimiseks kasutatakse jQuerys", "a"=>2, "o"=> array("$.get(selektor)", "samat kirjapilti nagu puhtas JS-is document.getElementBy ..jne", "$(selektor) kirjapilti")),
	
	array("q"=>"eelnevas punktid mainitud selektor on", "a"=>2, "o"=> array("elemendi asukoht DOM-is nt document>div>div>p", "jQuery-spetsiifilised selektorid", "CSS selektorid")),

	array("q"=>"selekteeritud elemendi objektile saab rakendada jQuery meetodeid, mis", "a"=>0, "o"=> array("üldjuhul tagastavad pärast oma tegevuse lõppu mõjutatud elemendi objekti", "üldjuhul tagastavad pärast oma tegevuse lõppu kas true tegevuse õnnestumisel või false luhtumisel", "ei tagasta mingit väärtust")),
	
	array("q"=>"Eelnevast punktist tulenevalt", "a"=>1, "o"=> array( "ei saa jQuery meetodeid rakendada ahelas, vaid tuleb iga meetodi käivitamisel objektile viitama (el.meetod; el.meetod jne)", "saab jQuery meetodeid rakendada ahelas (el.meetod.meetod.meetod)")),
	
	array("q"=>"objektid <u>this</u> ja <u>$(this)</u>", "a"=>0, "o"=> array("on erinevas vormingus ja seetõttu mittevõrdsed objektid, mis viitavad elemendile/objektile, millega seoses funktsioon välja kutsuti", "on võrdväärsed objektid (kasuta kumba kirjapilti tahad), mis viitavad elemendile/objektile, millega seoses funktsioon välja kutsuti")),

	array("q"=>"HTML elemendi atribuutide jQuery objekti kaudu mõjutamiseks", "a"=>1, "o"=> array("on kasutusel atribuudile vastavad meetodid nt .src() lugemiseks ja .src('uus') pildi aadressi kirjutamiseks", "on kasutusel .attr() meetod, mis võimaldab nii atribuudi väärtust lugeda (ainsaks parameetriks atribuudi nimi) kui ka väärtustada (lisaks veel teine parameeter väärtusega)", "kasutatakse puhta JS-ga samat kirjapilti nt el.src='uus' ")),
	
	array("q"=>"elemendi objekti sündmuse kirjeldamiseks kasutatakse jQuerys", "a"=>2, "o"=> array("enamasti sündmusatribuudi väärtuse määramist .attr meetodi abil", "enamasti vastava sündmuse meetodit nagu .onclick, .onmouseenter, .onchange", "enamasti vastava sündmuse meetodit nagu .click, .mouseenter, .change")),
	
	array("q"=>"elemendi stiilireeglite jQuery objekti kaudu mõjutamiseks", "a"=>0, "o"=> array("on kasutusel .css() meetod, millel on mitu erinevat kirjapilti omaduse väärtuse lugemiseks, kirjutamiseks ja mitme omaduse korraga mõjutamiseks", "kasutatakse puhta JS-ga samat kirjapilti nt. el.style.border='solid red'", "on kasutusel CSS omadustele vastavad meetodid nt .color() väärtuse lugemiseks ja .color('red') määramiseks")),

	
	array("q"=>"korrektne viis kasutada .css() meetodit", "a"=>4, "o"=> array("$('div').css(\"backgroundColor\")", "$('div').css(\"backgroundColor\", \"red\")", "$('div').css({\"backgroundColor\": \"red\", \"color\": \"white\"})", "$('div').css({backgroundColor: \"red\", color: \"white\"})","kõik loetletud on korrektsed")),
	
	array("q"=>"meetod .show(kiirus)", "a"=>1, "o"=> array("määrab elemendi visibility omaduse väärtuseks visible", "määrab elemendi display omaduse väärtuseks tema algse väärtuse (kui on block element, siis block jne)")),
	
	array("q"=>"meetod .hide(kiirus)", "a"=>1, "o"=> array("määrab elemendi visibility omaduse väärtuseks hidden", "määrab elemendi display omaduse väärtuseks none")),
	
	array("q"=>"AJAX on", "a"=>2, "o"=> array("nõudepesuvahend", "eraldi programmeerimiskeel, mida saab kasutada koos jQueryga serverile päringute saatmiseks", "meetod, mid on kasutusel kasutaja veebilehitsejast serverile päringute saatmiseks nii, et laetud lehte ei laeta ümber (aadress ei muutu)"))


	);
?>

<p>Enesetest on mõeldud selleks, et saaksite kontrollida oma arusaama loengus räägitud teemadel. Tulemusi ei salvestata.<br/>Küsimuste arv: <?php echo count($test); ?></p>
<form action="?" method="POST" id="enesele">
	<?php
		foreach($test as $id => $question){
			echo "<fieldset><legend>{$question['q']}</legend>";
			foreach ($question["o"] as $o_id => $o_text ){
				$extra="";
				$checked="";
				if (isset($_POST['answers']) && isset($_POST['answers'][$id]) && $_POST['answers'][$id]==$o_id){
					$checked="checked";
					if ($question['a']==$o_id) $extra="class=\"correct\"";
					else $extra="class=\"incorrect\"";
				}
				echo "<input type=\"radio\" id=\"a{$id}-{$o_id}\" name=\"answers[$id]\" value=\"{$o_id}\" {$checked} /><label for=\"a{$id}-{$o_id}\" {$extra} >{$o_text}</label><br/>";
			}
			echo "</fieldset>";
		}
	?><br/>

</form>
<p>On ideid rohkemateks küsimusteks? Kirjutage julgelt tiia.tanav[at]itcollege.ee </p>

<div id="action"><input type="submit" value="Testin teadmisi" form="enesele" /></div>
</body>
</html>