<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Enesetest 01</title>
	<style type="text/css">
	fieldset {
		margin:5px;
	}
legend {font-weight: bold;}
.correct {
	color:green;
}
.incorrect {
	color:red;
}
#action {
	position: fixed;
	left:0px;
	right: 0px;
	bottom: 0px;
	padding:10px 100px;
	background: #800080;
	border-top:solid grey 1px;
}
body {
	padding-bottom:50px;
}
input[type="radio"] {
	margin-top: 10px;
}
	</style>
</head>
<body>
<?php

$test=array(
	array("q"=>"Elemendi artibuudid", "a"=>2, "o"=> array("kirjutatakse elemendi tagide vahele ning kirjeldavad elemendi lisaomadusi", "kirjutatakse ükskõik kumma tagi sisse ning kirjeldavad elemendi lisaomadusi", "kirjutatakse elemendi algustagi sisse ning kirjeldavad elemendi lisaomadusi")),

	array("q"=>"pildi elemendi lisamiseks kirjutan HTML dokumenti", "a"=>0, "o"=> array("&lt;img src=\"pildi/aadress.png\" /&gt;", "&lt;img&gt;pildi/aadress.png&lt;/img&gt;", "&lt;img src=\"pildi/aadress.png\" &gt;&lt;/img&gt;")),
	
	array("q"=>"Pildi elemendi atribuut src", "a"=>1, "o"=> array("on lühend sõnast 'search' ning sellega kirjeldatakse pildi veebist üles leidmiseks kasutatavaid otsingusõnu", "on lühend sõnast 'source' ning sellega kirjeldatakse kuvatava pildi asukohta veebis", "on lühend sõnast 'source' ning sellega kirjeldatakse kuvatava pildi asukohta arendaja arvutis")),
	
	array("q"=>"Valida korrektselt kirjutatud element", "a"=>2, "o"=> array("&lt;img src=\"pildi/aadress.png\" width=\"...\" height=\"...\" /&gt;", "&lt;img src=\"pildi/aadress.png\" width=\"100px\" height=\"200px\" /&gt;", "&lt;img src=\"pildi/aadress.png\" alt=\"katki :( \" width=\"100\" height=\"200\" /&gt;", "&lt;img src=\"pildi/aadress.png\" alt=\"katki :( \" width=\"100px\" height=\"200px\" /&gt;" , "&lt;img src=\"pildi/aadress.png\" alt=\"katki :( \" width=\"50%\" height=\"50%\" /&gt;")),

	array("q"=>"Pildil ei pea olema alt atribuuti", "a"=>1, "o"=> array("Tõepooles pole vaja", "on vaja juhuks kui pilti ei leita üles", "ega ta kahju ei tee, aga olemasolust eriti midagi ei muutu")),
	
	array("q"=>"HTML dokumenti koostades", "a"=>0, "o"=> array("on kasulik järgida kindlat struktuuri ja reegleid, kuidas HTML elemente tohib üksteise sisse paigutada", "ei pea muretsema reeglite pärast - elemente võib paigutada oma soovi järgi", "peab kindlasti järgima kindlat struktuuri ja reegleid, kuidas HTML elemente tohib üksteise sisse paigutada, muidu lehte ei kuvata")),
	
	array("q"=>"HTML dokumendis komentaaride lisamiseks kasutatakse", "a"=>3, "o"=> array("// üherealine kommentaar", "/* mitmerealine kommentaar*/", "# üherealine kommentaar", "&lt;!-- mitmerealine kommentaar --&gt;", "&lt;comment&gt;kommentaari element&lt;/comment&gt;")),
	
	array("q"=>"HTML dokumendi päisesse (&lt;head&gt; ja &lt;/head &gt; vahele) ei kirjutata", "a"=>2, "o"=> array("lehe kohta käivat lisa(meta) infot nagu kooditabel, autor, märksõnad", "lehe stiili (css)", "lehel kuvatavat sisu", "Lehel kasutuses olevaid interaktiivsust võimaldavaid skripte (JS)")),
	
	array("q"=>"&lt;title&gt; element", "a"=>0, "o"=> array("paigutatakse alati HTML dokumendi päisesse (head) ning selles olevat teksti kuvatakse veebilehitseja tab-il, menüüriba kohal, järjehoidjates ...", "on kasutusel HTML dokumendi kehaosas (body), kus selle vahel olev tekst kuvatakse eriti suure pealkirjana", "ei eksisteeri")),

	array("q"=>"Pealkirjade puhul", "a"=>1, "o"=> array("on h6 element suurema tekstiga kui h1", "on h1 element suurema tekstiga kui h6", "on kõigil h1 - h6 elementidel tekstisuurus sama, ainuke erinevus on teksti kohal ja all olevas tühjas ruumis")),

	array("q"=>"Selleks et lehel kuvataks reavahetust (elementide või teksti vahel)", "a"=>0, "o"=> array("on vaja konkreetse koha peale kirjutada reavahetust tekitava &lt;br/&gt;elemendi (break)", "on vaja teksti sisse enteri abil reavahetusi lisada", "tuleb kirjutada &#92;n või &#92;r")),

	array("q"=>"nimekirja stiili määrab", "a"=>0, "o"=> array("nimekirja element - kas ol (ordered list) või ul (unordered list)", "nimekirja sees olevad elemendid", "sõltumata nii ol kui ka ul nimekirja puhul tuleb stiil alati määrata CSS stiilireeglitega")),

	array("q"=>"nii ul kui ka ol listielemendi sisse", "a"=>2, "o"=> array("võib panna ükskõik milliseid teisi HTML elemente", "tohib panna ainult inline tüüpi elemente", "tohib panna ainult li (list item) elemente, mille sisse sobib panna teisi elemente", "tohib panna ainult block tüüpi elemente")),

	array("q"=>"paragrahvi elemendi sisse tohib paigutada", "a"=>1, "o"=> array("ainult teksti", "ainult teksti ja inline tüüpi elemente nagu pildid, lingid, stiliseerimiselemendid (bold, italic jne)", "pilte, linke, stiilielemente, liste ja teisi paragrahve")),

	array("q"=>"Korrektne viis lehele linke lisada on", "a"=>2, "o"=> array("&lt;a&gt;http://kala.ee&lt;/a&gt;", "&lt;a src=\"http://kala.ee\" /&gt;", "&lt;a href=\"http://kala.ee\" &gt;lingi tekst&lt;/a&gt;", "&lt;a src=\"http://kala.ee\" &gt;lingi tekst&lt;/a&gt;")),

	array("q"=>"aadress 'kalad/haug.html' on ", "a"=>1, "o"=> array("faili täispikk aadress", "faili suhtelise failiteega aadress", "faili absoluutse failiteega aadress")),
	
	array("q"=>"viide <u>/pildid/haug.png</u> täispika aadressina <u>http://kalad.ee/kalad/haug.html</u> lehel on ", "a"=>2, "o"=> array("http://kalad.ee/kalad/pildid/haug.png", "http://kalad.ee/kalad/haug.html/pildid/haug.png", "http://kalad.ee/pildid/haug.png"))
	);


?>
<p>Enesetest on mõeldud selleks, et saaksite kontrollida oma arusaama loengus räägitud teemadel. Tulemusi ei salvestata.<br/>Küsimuste arv: <?php echo count($test); ?></p>
<form action="?" method="POST" id="enesele">
	<?php
		foreach($test as $id => $question){
			echo "<fieldset><legend>{$question['q']}</legend>";
			foreach ($question["o"] as $o_id => $o_text ){
				$extra="";
				$checked="";
				if (isset($_POST['answers']) && isset($_POST['answers'][$id]) && $_POST['answers'][$id]==$o_id){
					$checked="checked";
					if ($question['a']==$o_id) $extra="class=\"correct\"";
					else $extra="class=\"incorrect\"";
				}
				echo "<input type=\"radio\" id=\"a{$id}-{$o_id}\" name=\"answers[$id]\" value=\"{$o_id}\" {$checked} /><label for=\"a{$id}-{$o_id}\" {$extra} >{$o_text}</label><br/>";
			}
			echo "</fieldset>";
		}
	?><br/>

</form>
<p>On ideid rohkemateks küsimusteks? Kirjutage julgelt tiia.tanav[at]itcollege.ee </p>
<div id="action"><input type="submit" value="Testin teadmisi" form="enesele" /></div>
</body>
</html>
