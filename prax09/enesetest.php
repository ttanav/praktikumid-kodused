<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Enesetest 09</title>
	<script
src="http://code.jquery.com/jquery-1.11.0.min.js">
</script>
	<style type="text/css">
	fieldset {
		margin:5px;
	}
.correct {
	color:green;
}
.incorrect {
	color:red;
}
#action {
	position: fixed;
	left:0px;
	right: 0px;
	bottom: 0px;
	padding:10px 100px;
	background: #800080;
	border-top:solid grey 1px;
}
body {
	padding-bottom:50px;
}
input[type="radio"] {
	margin-top: 10px;
}
legend {font-weight: bold;}
	</style>
</head>
<body>
<?php // &lt;&gt;

$test=array(
	array("q"=>"Lühend MVC on pikalt", "a"=>1, "o"=> array("Multiview Video Coding", "Model View Conrtoller", "Multivariable control")),
	
	array("q"=>"Selle eesmärgiks on", "a"=>0, "o"=> array("eraldada äriloogika, kujundus ja juhtloogika", "eraldada eri keeltes kirjutatud leheosi")),
	
	array("q"=>"Mudel sisaldab", "a"=>0, "o"=> array("äriloogikat - info hankimine, töötlemine, edastamine", "HTML koodi, mis on täiendatud PHP väljaprintidega", "juhtloogikat - koodi, mis otsutab mida mingi päringu puhul teha/kuvada")),
	
	array("q"=>"Vaade sisaldab", "a"=>1, "o"=> array("äriloogikat - info hankimine, töötlemine, edastamine", "HTML koodi, mis on täiendatud PHP väljaprintidega", "juhtloogikat - koodi, mis otsutab mida mingi päringu puhul teha/kuvada")),
	
	array("q"=>"Kontroller sisaldab", "a"=>2, "o"=> array("äriloogikat - info hankimine, töötlemine, edastamine", "HTML koodi, mis on täiendatud PHP väljaprintidega", "juhtloogikat - koodi, mis otsutab mida mingi päringu puhul teha/kuvada")),
	
	array("q"=>"Selleks, et teksti URL sisse sobivaks saada kasutatakse", "a"=>1, "o"=> array("htmlspecialchars(&#36;tekst); funktsiooni", "urlencode(&#36;tekst); funktsiooni", "urlescape(&#36;tekst); funktsiooni")),
	
	array("q"=>"Selleks, et stringis olevad noolsulud jm HTML jaoks tähenduslikud märgid kuvataks teksti osana, kasutatakse", "a"=>0, "o"=> array("htmlspecialchars(&#36;tekst); funktsiooni", "urlencode(&#36;tekst); funktsiooni", "urlescape(&#36;tekst); funktsiooni")),
	
	array("q"=>"htmlspecialchars funktsiooni kasutamine aitab vältida", "a"=>2, "o"=> array("DDOS rünnet", "SQL injection rünnet", "code injection rünnet")),
	
	array("q"=>"is_numeric(\"0xf4c3b00c\"); tagastab", "a"=>0, "o"=> array("true", "false", "numbri kümnendsüsteemis")),
	
	array("q"=>"is_int(\"23\"); tagastab ", "a"=>1, "o"=> array("true", "false")),
	
	array("q"=>"HTML päringu päiste väärtuste muutmiseks kasutatakse", "a"=>0, "o"=> array("header(\"päis: väärtus\"); funkstiooni", "head(\"päis: väärtus\"); funkstiooni", "set_header(\"päis: väärtus\"); funkstiooni")),
	
	array("q"=>"eelnevalt mainitud funktsiooni väljakutse", "a"=>1, "o"=> array("võib olla ükskõik kus PHP koodis", "võib olla PHP koodis suvalises kohal kus pole eelnevalt midagi väljastatud", "peab alati olema PHP koodi alguses esimesel real")),
	
	array("q"=>"Kasutaja ümbersuunamiseks seatakse päis", "a"=>2, "o"=> array("location: http://uuskoht.ee", "URL: http://uuskoht.ee", "Location: http://uuskoht.ee")),
	array("q"=>"viitmuutuja on", "a"=>1, "o"=> array("muutuja, mis viitab oma väärtusele", "muutuja, mis viitab mõne teise muutujaga samale mälupesale/väärtusele ning kummagi muutmisel on mõjutatud kõikide väärtused.", "muutuja, mis viitab mõne teise muutujaga samale mälupesale/väärtusele ning kummagi muutmisel on mõjutatud vaid muudetud muutuja enda väärtus")),
	
	array("q"=>"viitmuutuja loomisel pannakse tema ette ", "a"=>2, "o"=> array("? küsimärk", "!hüüumärk", " & ampersand")),
	
	array("q"=>"koodi <pre>&#36;mingi=array(1,2,3,4); 
foreach(&#36;mingi as &&#36;a){
	&#36;a*=2;
}</pre>tulemusena on massiivis väärtused", "a"=>0, "o"=> array("2, 4, 6, 8", "2, 2, 2, 2", "1, 2, 3, 4"))

	);
?>
<p>Enesetest on mõeldud selleks, et saaksite kontrollida oma arusaama loengus räägitud teemadel. Tulemusi ei salvestata.<br/>Küsimuste arv: <?php echo count($test); ?></p>
<form action="?" method="POST" id="enesele">
	<?php
		foreach($test as $id => $question){
			echo "<fieldset><legend>{$question['q']}</legend>";
			foreach ($question["o"] as $o_id => $o_text ){
				$extra="";
				$checked="";
				if (isset($_POST['answers']) && isset($_POST['answers'][$id]) && $_POST['answers'][$id]==$o_id){
					$checked="checked";
					if ($question['a']==$o_id) $extra="class=\"correct\"";
					else $extra="class=\"incorrect\"";
				}
				echo "<input type=\"radio\" id=\"a{$id}-{$o_id}\" name=\"answers[$id]\" value=\"{$o_id}\" {$checked} /><label for=\"a{$id}-{$o_id}\" {$extra} >{$o_text}</label><br/>";
			}
			echo "</fieldset>";
		}
	?><br/>

</form>
<p>On ideid rohkemateks küsimusteks? Kirjutage julgelt tiia.tanav[at]itcollege.ee </p>

<div id="action"><input type="submit" value="Testin teadmisi" form="enesele" /></div>
</body>
</html>
