<?php
require_once("functions.php");
$pildid=array(
		array("big"=>"Pictures/caution.gif", "small"=>"Thumbs/caution_small.gif", "alt"=>"Ettevaatust lapsed"),
		array("big"=>"Pictures/slow.gif", "small"=>"Thumbs/slow_small.gif", "alt"=>"Aeglasemalt paluks"),
		array("big"=>"Pictures/closed.jpg", "small"=>"Thumbs/closed_small.jpg", "alt"=>"Oleme kinni"),
		array("big"=>"Pictures/fish.jpg", "small"=>"Thumbs/fish_small.jpg", "alt"=>"Kalastamine keelatud"),
		array("big"=>"Pictures/fly.jpg", "small"=>"Thumbs/fly_small.jpg", "alt"=>"Õpi lendama"),
		array("big"=>"Pictures/wip.png", "small"=>"Thumbs/wip_small.png", "alt"=>"Töö pooleli")	,
		array("big"=>"Pictures/sign.jpg", "small"=>"Thumbs/signThumb.jpg", "alt"=>"Unista edasi")	
	);

$mode="";
if (!empty($_GET['mode'])){
	$mode=$_GET['mode'];
}

switch($mode){
	case "galerii":
		kuva_galerii();
	break;
	case "pilt":
		kuva_pilt();
	break;
	case "login":
		kuva_login();
	break;
	case "register":
		kuva_register();
	break;
	case "pildivorm":
		kuva_pildivorm();
	break;
	default:
		kuva_pealeht();
	break;
}


?>