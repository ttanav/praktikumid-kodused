<?php
function alusta_sessioon(){
	// siin ees võiks muuta ka sessiooni kehtivusaega, aga see pole hetkel tähtis
	session_start();
}
	
function lopeta_sessioon(){
	$_SESSION = array();
	if (isset($_COOKIE[session_name()])) {
 	 setcookie(session_name(), '', time()-42000, '/');
	}
	session_destroy();
}

function connect_db(){
  global $connection;
  $host="localhost";
  $user="test";
  $pass="t3st3r123";
  $db="test";
  $connection = mysqli_connect($host, $user, $pass, $db) or die("ei saa ühendust mootoriga- ".mysqli_error());
  mysqli_query($connection, "SET CHARACTER SET UTF8") or die("Ei saanud baasi utf-8-sse - ".mysqli_error($connection));
}


function on_sees(){
	if (empty($_SESSION['user'])) {
		$_SESSION['notices'][]="Piiratud ligipääs";
		header("Location: ?mode=login");
		exit(0);
	}
}

function hangi_pildid(){
	global $connection;
	$pildid=array();
	$sql="SELECT p.*, u.username as author FROM pildid_14 p, kasutajad_14 u where u.id=p.kasutaja_id";
	$result=mysqli_query($connection, $sql);
	while ($row=mysqli_fetch_assoc($result)){
		$row['alt']="{$row['author']} - {$row['title']}";
		$pildid[]=$row;
	}
	return $pildid;

}

function koik_id(){
	global $connection;
	$id=array();
	$sql="SELECT id FROM pildid_14";
	$result=mysqli_query($connection, $sql);
	while ($row=mysqli_fetch_assoc($result)){
		$id[]=$row['id'];
	}
	return $id;

}

function hangi_pilt($id){
	global $connection;
	$pilt=array();
	$id=mysqli_real_escape_string($connection, $id);
	$sql="SELECT p.*, u.username as author FROM pildid_14 p, kasutajad_14 u where p.id=$id and u.id=p.kasutaja_id";
	$result=mysqli_query($connection, $sql);
	while ($row=mysqli_fetch_assoc($result)){
		$row['alt']="{$row['author']} - {$row['title']}";
		$pilt=$row;
	}
	return $pilt;
}


function kuva_galerii(){
	$pildid = hangi_pildid();
	include_once("views/head.html");
	include("views/galerii.html");
	include_once("views/foot.html");
}

function kuva_pilt(){
	$pildid = hangi_pildid();
	$ids = koik_id();
	$id=0;
	if (!empty($_GET['id']) && !empty($ids[$_GET['id']]) ){
		$id=$_GET['id'];
	}
	$eelmine=$id-1;
	if ($eelmine<0) $eelmine=count($ids)-1;
	$jargmine = $id+1;
	if ($jargmine==count($pildid)) $jargmine=0;
	$pilt=hangi_pilt($ids[$id]);
	include("views/pilt.html");
}

function tee_logout(){
	lopeta_sessioon();
	header("Location: ?");
	exit(0);
}

function kuva_login(){
	global $connection;
	if (!empty($_POST)){
		$errors=array();
		if (empty($_POST['username'])) {
			$errors["un"]="Kasutajanimi vajalik";
		}
		if (empty($_POST['passwd'])){
			$errors["pw"]="parool vajalik";
		}
		if (empty($errors)){
			$user=mysqli_query($connection, "SELECT id, username, roll from kasutajad_14 WHERE username='".mysqli_real_escape_string($connection, $_POST['username'])."' and passw=sha1('".mysqli_real_escape_string($connection, $_POST['passwd'])."')");
			$u=mysqli_fetch_assoc($user);
			if ($u){
				$_SESSION['user']=$u['username'];
				$_SESSION['user_id']=$u['id'];
				$_SESSION['roll']=$u['roll'];
				$_SESSION['notice']="sisselogimine edukas";
				header("Location: ?mode=galerii");
				exit(0);
			} else {
				$errors["wr"]="vale info. Proovi uuesti";
			}
		}
	}

	include_once("views/head.html");
	include("views/login.html");
	include_once("views/foot.html");
}

function kuva_register(){
	include_once("views/head.html");
	include("views/register.html");
	include_once("views/foot.html");
}

function kuva_pildivorm(){
	global $connection;
	on_sees();
	$errors=array();
	$pildid=hangi_pildid();
	$pilt=array();
	if (!empty($_GET['id'])){ // kui on ok id urlis siis teha massiiv
		$pilt=hangi_pilt( $_GET['id'] ); // kas tühimassiiv või täis massiiv
	}
	if (!empty($pilt) && ($pilt['kasutaja_id']!=$_SESSION['user_id'] && $_SESSION['roll']!="admin")) {
			$_SESSION['notice']="Sa saad muuta ainult enda pilte!";
			header("Location: ?mode=galerii");
			exit(0);
		
	}
	// siit alates 13. prax
	if (!empty($_POST)){
		if (empty($_POST['pealkiri'])) {
			$errors['pk']="pildil peab olema pealkiri";
		} else{
			$pilt['title']=mysqli_real_escape_string($connection, $_POST['pealkiri']);
		}
			
		if (empty($errors)){
			// ok, lae üles või uuenda
			$url = upload('suur', "Pictures");
			if ($url!="") $pilt['big']=mysqli_real_escape_string($connection,$url);
			
			if (empty($_GET['id']) && empty($pilt['big'])) {
				// uus pilt, kohustuslik
				$errors['s']="Suur pilt on kohustuslik";
			}
			$url = upload('v2ike', "Thumbs");
			if ($url!="") $pilt['small']=mysqli_real_escape_string($connection,$url);
			
			if (empty($_GET['id']) && empty($pilt['small'])) {
				// uus pilt, kohustuslik
				$errors['v']="Väike pilt on kohustuslik";
			}

			if (empty($errors)){
				// indert or update
				if (empty($_GET['id'])){
					$pilt['kasutaja_id']=$_SESSION['user_id'];
					$sql="insert into pildid_14 (kasutaja_id, title, big, small) values ('{$pilt['kasutaja_id']}', '{$pilt['title']}', '{$pilt['big']}', '{$pilt['small']}')";
				} else {
					$sql="update pildid_14 set title='{$pilt['title']}', big='{$pilt['big']}', small='{$pilt['small']}' where id=".mysqli_real_escape_string($connection,$_GET['id']);
				}
				$result=mysqli_query($connection, $sql);
				if ($result){
					// õnnestus
					echo $result;
					$_SESSION['notice']="pildi laadimine/muutmine õnnestus";
					header("Location: ?mode=galerii");
					exit(0);
				} else {
					//failed
					$_SESSION['notice']="pildi laadimine/muutmine luhtus";
				}

			}
			
		}
	}
	include_once("views/head.html");
	include("views/pildivorm.html");
	include_once("views/foot.html");
}


function kuva_pealeht(){
	include_once("views/head.html");
	include("views/pealeht.html");
	include_once("views/foot.html");
}


function upload($name, $loc){
  $allowedExts = array("jpg", "jpeg", "gif", "png");
  $allowedTypes = array("image/gif", "image/jpeg", "image/png","image/pjpeg");
  $extension = end(explode(".", $_FILES[$name]["name"]));

  if ( in_array($_FILES[$name]["type"], $allowedTypes)
   && ($_FILES[$name]["size"] < 100000)
   && in_array($extension, $allowedExts)) {
    // fail õiget tüüpi ja suurusega
    if ($_FILES[$name]["error"] > 0) {
      return "";
    } else {
      // vigu ei ole
      if (file_exists($loc."/" . $_FILES[$name]["name"])) {
        // fail olemas ära uuesti lae, tagasta failinimi
        return $_FILES[$name]["name"];
      } else {
        // kõik ok, aseta pilt
        move_uploaded_file($_FILES[$name]["tmp_name"], $loc."/" . $_FILES[$name]["name"]);
        return $_FILES[$name]["name"];
      }
    }
  } else {
    return "";
  }
}

?>