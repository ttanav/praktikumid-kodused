<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>Praktikum  - Ülesanne</title>
</head>
<body>
<?php 
// massiiv
$massiiv =array(
		array("Nimi"=>"Sofia", "Vanus"=>"4", "Värvus"=>"kollane", "Iseloom"=>"meeldib asju ümber ajada"),
		array("Nimi"=>"Peeter", "Vanus"=>"2", "Värvus"=>"must", "Iseloom"=>"ei meeldi läikivad mänguasjad"),
		array("Nimi"=>"Tom", "Vanus"=>"6", "Värvus"=>"hall", "Iseloom"=>"on väga raske sobivat toitu leida"),
		array("Nimi"=>"Muira", "Vanus"=>"8", "Värvus"=>"laiguline", "Iseloom"=>"meeldivad lapsed"),
		array("Nimi"=>"Nurr", "Vanus"=>"1", "Värvus"=>"mustvalge", "Iseloom"=>"ei tasu lähedale minna")
	);
?>

<?php foreach($massiiv as $id=>$value) include('korduv.html'); ?>	

</body>
</html>