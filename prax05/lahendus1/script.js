var info={
	"1": {"img": "Pictures/sign.jpg", "autor":"Keegi inimene", "pealkiri":"mingi silt on 1"},
	"2": {"img": "Pictures/pilt.png", "autor":"Keegi inimene", "pealkiri":"mingi silt on 2"},
	"3": {"img": "Pictures/sign.jpg", "autor":"Keegi inimene", "pealkiri":"mingi silt on 3"},
	"4": {"img": "Pictures/sign.jpg", "autor":"Keegi inimene", "pealkiri":"mingi silt on 4"},
	"5": {"img": "Pictures/sign.jpg", "autor":"Keegi inimene", "pealkiri":"mingi silt on 5"}
}

window.onload = function() {

galerii=document.getElementById('galerii');
if (typeof(galerii) != 'undefined' && galerii != null) {	
	// lisada sündmus
	lingid=galerii.getElementsByTagName("img");
	for (i=0; i<lingid.length; i++){
		lingid[i].data=i;
		lingid[i].onclick=function(){
			showDetails(this.data);
		}
	}
}

}
function showDetails(id){
	id=id+1;
	hoidja=document.getElementById('hoidja');
	if (typeof(hoidja) != 'undefined' && hoidja != null) {
		document.getElementById("suurpilt").src=info[id]['img'];
		document.getElementById("inf").innerHTML=info[id]['autor']+" - "+info[id]['pealkiri'];
		hoidja.style.display="initial";
	}

}
function hideDetails() {
	hoidja=document.getElementById('hoidja');
	if (typeof(hoidja) != 'undefined' && hoidja != null) {
		hoidja.style.display="none";
	}
}