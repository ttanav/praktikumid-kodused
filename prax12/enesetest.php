
<!DOCTYPE>
<html>
<header>
	<meta charset="utf-8" />
	<title>Enesetest 12</title>
	<script
src="http://code.jquery.com/jquery-1.11.0.min.js">
</script>
	<style type="text/css">
	fieldset {
		margin:5px;
	}
.correct {
	color:green;
}
.incorrect {
	color:red;
}
#action {
	position: fixed;
	left:0px;
	right: 0px;
	bottom: 0px;
	padding:10px 100px;
	background: #800080;
	border-top:solid grey 1px;
}
body {
	padding-bottom:50px;
}
input[type="radio"] {
	margin-top: 10px;
}
legend {font-weight: bold;}
	</style>
</header>
<body>
<?php // &lt;&gt;  &#36;

$test=array(
	array("q"=>"Mysql andmbaasiga ühendumiseks kasutatakse funktsiooni", "a"=>1, "o"=> array("connect( &#36;host,  &#36;user,  &#36;password);","mysqli_connect( &#36;host,  &#36;user,  &#36;password,  &#36;database);","mysqli_connect( &#36;host,  &#36;database, &#36;user,  &#36;password);")),
	
	array("q"=>"kui eelneva funktsiooni poolt tagastatav väärtus salvestada muutujasse &#36;data, on selle muutuja vääärtuseks", "a"=>0, "o"=> array("andmebaasiühenduse objekti", "vastavalt õnnestumisele või luhtumisele TRUE või FALSE", "andmebaasiühenduse identifikaator")),
	
	array("q"=>"andmebaasiühenduse käigus saadud viimase veateate saab kätte funktsiooniga", "a"=>2, "o"=> array("error(&#36;data);", "mysql_error(&#36;data);", "mysqli_error(&#36;data);")),
	
	array("q"=>"erinavate SQL päringute käivitamiseks on funktsioon", "a"=>0, "o"=> array("mysqli_query(&#36;data, 'päring');", "mysql_query(&#36;data, 'päring');", "mysqli_query('päring');")),
	
	array("q"=>"SELECT päringu käivitamisel tagastab see funktsioon", "a"=>1, "o"=> array("õnnestumisel TRUE, vastasel juhul FALSE", "õnestumisel tulemuste objekti, luhtumisel FALSE", "õnestumisel kahemõõtmelise massiivi tulemustega, luhtumisel FALSE")),
	
	array("q"=>"SHOW päringu käivitamisel tagastab see funktsioon", "a"=>1, "o"=> array("õnnestumisel TRUE, vastasel juhul FALSE", "õnestumisel tulemuste objekti, luhtumisel FALSE", "õnestumisel kahemõõtmelise massiivi tulemustega, luhtumisel FALSE")),
	
	array("q"=>"DELETE  päringu käivitamisel tagastab see funktsioon", "a"=>0, "o"=> array("õnnestumisel TRUE, vastasel juhul FALSE", "õnestumisel tulemuste objekti, luhtumisel FALSE", "õnestumisel kahemõõtmelise massiivi tulemustega, luhtumisel FALSE")),
	
	array("q"=>"DESCRIBE päringu käivitamisel tagastab see funktsioon", "a"=>1, "o"=> array("õnnestumisel TRUE, vastasel juhul FALSE", "õnestumisel tulemuste objekti, luhtumisel FALSE", "õnestumisel kahemõõtmelise massiivi tulemustega, luhtumisel FALSE")),
	
	array("q"=>"INSERT päringu käivitamisel tagastab see funktsioon", "a"=>0, "o"=> array("õnnestumisel TRUE, vastasel juhul FALSE", "õnestumisel tulemuste objekti, luhtumisel FALSE", "õnestumisel kahemõõtmelise massiivi tulemustega, luhtumisel FALSE")),
	
	array("q"=>"UPDATE päringu käivitamisel tagastab see funktsioon", "a"=>0, "o"=> array("õnnestumisel TRUE, vastasel juhul FALSE", "õnestumisel tulemuste objekti, luhtumisel FALSE", "õnestumisel kahemõõtmelise massiivi tulemustega, luhtumisel FALSE")),
	
	array("q"=>"pärast päringu käivitamist tulemuste lugemiseks (eeldusel, et tulemus salvestati muutujasse  &#36;result) kasutatakse funktsiooni", "a"=>2, "o"=> array("mysqli_fetch_array(&#36;result);", "mysqli_fetch_assoc(&#36;result);", "mõlemat ülevalmainitut")),
	
	array("q"=>"mysqli_fetch_array(&#36;result); tagastab", "a"=>2, "o"=> array("1 rea massiivi, kus tabelist päritud väljad tekstiliste võtmetega VÕI, kui ridu rohkem pole, NULL", "1 rea massiivi, kus tabelist päritud väljad on numbriliste võtmetega VÕI, kui ridu rohkem pole, NULL", "1 rea massiivi, kus tabelist päritud väljad on numbriliste ja tekstiliste võtmetega (topelt info) VÕI, kui ridu rohkem pole, NULL")),
	
	array("q"=>"mysqli_fetch_assoc(&#36;result); tagastab", "a"=>0, "o"=> array("1 rea massiivi, kus tabelist päritud väljad tekstiliste võtmetega VÕI, kui ridu rohkem pole, NULL", "1 rea massiivi, kus tabelist päritud väljad on numbriliste võtmetega VÕI, kui ridu rohkem pole, NULL", "1 rea massiivi, kus tabelist päritud väljad on numbriliste ja tekstiliste võtmetega (topelt info) VÕI, kui ridu rohkem pole, NULL")),
	
	array("q"=>"funktsioon mysqli_free_result(&#36;result); tulemusena", "a"=>1, "o"=> array("kustutatakse muutuja (sama mis unset(&#36;result);)", "vabastatakse päringu tulemuse objekti poolt hõivatud mälu, muutuja jääb alles", "vabastatakse päringu tulemuse objekti poolt hõivatud mälu, muutuja kustutatakse")),
	
	array("q"=>"funktsioon mysqli_close(&#36;data); tulemusena", "a"=>1, "o"=> array("kustutatakse muutuja (sama mis unset(&#36;data);)", "lõpetatakse ühendus andmebaasiga, muutuja jääb alles", "lõpetatakse ühendus andmebaasiga, muutuja kustutatakse"))

	);
?>
<p>Enesetest on mõeldud selleks, et saaksite kontrollida oma arusaama loengus räägitud teemadel. Tulemusi ei salvestata.<br/>Küsimuste arv: <?php echo count($test); ?></p>
<form action="?" method="POST" id="enesele">
	<?php
		foreach($test as $id => $question){
			echo "<fieldset><legend>{$question['q']}</legend>";
			foreach ($question["o"] as $o_id => $o_text ){
				$extra="";
				$checked="";
				if (isset($_POST['answers']) && isset($_POST['answers'][$id]) && $_POST['answers'][$id]==$o_id){
					$checked="checked";
					if ($question['a']==$o_id) $extra="class=\"correct\"";
					else $extra="class=\"incorrect\"";
				}
				echo "<input type=\"radio\" id=\"a{$id}-{$o_id}\" name=\"answers[$id]\" value=\"{$o_id}\" {$checked} /><label for=\"a{$id}-{$o_id}\" {$extra} >{$o_text}</label><br/>";
			}
			echo "</fieldset>";
		}
	?><br/>

</form>
<p>On ideid rohkemateks küsimusteks? Kirjutage julgelt tiia.tanav[at]itcollege.ee </p>

<div id="action"><input type="submit" value="Testin teadmisi" form="enesele" /></div>
</body>
</html>