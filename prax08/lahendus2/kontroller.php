<?php

$pildid=array(
		array("big"=>"Pictures/caution.gif", "small"=>"Thumbs/caution_small.gif", "alt"=>"Ettevaatust lapsed"),
		array("big"=>"Pictures/slow.gif", "small"=>"Thumbs/slow_small.gif", "alt"=>"Aeglasemalt paluks"),
		array("big"=>"Pictures/closed.jpg", "small"=>"Thumbs/closed_small.jpg", "alt"=>"Oleme kinni"),
		array("big"=>"Pictures/fish.jpg", "small"=>"Thumbs/fish_small.jpg", "alt"=>"Kalastamine keelatud"),
		array("big"=>"Pictures/fly.jpg", "small"=>"Thumbs/fly_small.jpg", "alt"=>"Õpi lendama"),
		array("big"=>"Pictures/wip.png", "small"=>"Thumbs/wip_small.png", "alt"=>"Töö pooleli")	,
		array("big"=>"Pictures/sign.jpg", "small"=>"Thumbs/signThumb.jpg", "alt"=>"Unista edasi")	
	);

$mode="";
if (!empty($_GET['mode'])){
	$mode=$_GET['mode'];
}

switch($mode){
	case "galerii":
		include_once("views/head.html");
		include("views/galerii.html");
		include_once("views/foot.html");
	break;
	case "pilt":
		$id=0;
		if (!empty($_GET['id']) && is_numeric($_GET['id']) && isset($pildid[$_GET['id']]) ){
			$id=$_GET['id'];
		}
		$eelmine=$id-1;
		if ($eelmine<0) $eelmine=count($pildid)-1;
		$jargmine = $id+1;
		if ($jargmine==count($pildid)) $jargmine=0;
		$pilt=$pildid[$id];
		include("views/pilt.html");
	break;
	case "login":
		include_once("views/head.html");
		include("views/login.html");
		include_once("views/foot.html");
	break;
	case "register":
		include_once("views/head.html");
		include("views/register.html");
		include_once("views/foot.html");
	break;
	case "pildivorm":
		include_once("views/head.html");
		include("views/pildivorm.html");
		include_once("views/foot.html");
	break;
	default:
		include_once("views/head.html");
		include("views/pealeht.html");
		include_once("views/foot.html");
	break;
}


?>