<?php
function alusta_sessioon(){
	// siin ees võiks muuta ka sessiooni kehtivusaega, aga see pole hetkel tähtis
	session_start();
}
	
function lopeta_sessioon(){
	$_SESSION = array();
	if (isset($_COOKIE[session_name()])) {
 	 setcookie(session_name(), '', time()-42000, '/');
	}
	session_destroy();
}

function connect_db(){
  global $connection;
  $host="localhost";
  $user="test";
  $pass="t3st3r123";
  $db="test";
  $connection = mysqli_connect($host, $user, $pass, $db) or die("ei saa ühendust mootoriga- ".mysqli_error());
  mysqli_query($connection, "SET CHARACTER SET UTF8") or die("Ei saanud baasi utf-8-sse - ".mysqli_error($connection));
}


function on_sees(){
	if (empty($_SESSION['user'])) {
		$_SESSION['notices'][]="Piiratud ligipääs";
		header("Location: ?mode=login");
		exit(0);
	}
}

function hangi_pildid(){
	global $connection;
	$pildid=array();
	$sql="SELECT * FROM pildid_12";
	$result=mysqli_query($connection, $sql);
	while ($row=mysqli_fetch_assoc($result)){
		//$row['alt']="{$row['author']} - {$row['title']}";
		$pildid[]=$row;
	}
	return $pildid;

}

function koik_id(){
	global $connection;
	$id=array();
	$sql="SELECT id FROM pildid_12";
	$result=mysqli_query($connection, $sql);
	while ($row=mysqli_fetch_assoc($result)){
		$id[]=$row['id'];
	}
	return $id;

}

function hangi_pilt($id){
	global $connection;
	$pilt=array();
	$id=mysqli_real_escape_string($connection, $id);
	$sql="SELECT * FROM pildid_12 where id=$id";
	$result=mysqli_query($connection, $sql);
	while ($row=mysqli_fetch_assoc($result)){
		//$row['alt']="{$row['author']} - {$row['title']}";
		$pilt=$row;
	}
	return $pilt;
}


function kuva_galerii(){
	$pildid = hangi_pildid();
	include_once("views/head.html");
	include("views/galerii.html");
	include_once("views/foot.html");
}

function kuva_pilt(){
	$pildid = hangi_pildid();
	$ids = koik_id();
	$id=0;
	if (!empty($_GET['id']) && !empty($ids[$_GET['id']]) ){
		$id=$_GET['id'];
	}
	$eelmine=$id-1;
	if ($eelmine<0) $eelmine=count($ids)-1;
	$jargmine = $id+1;
	if ($jargmine==count($pildid)) $jargmine=0;
	$pilt=hangi_pilt($ids[$id]);
	include("views/pilt.html");
}

function tee_logout(){
	lopeta_sessioon();
	header("Location: ?");
	exit(0);
}

function kuva_login(){
	if (!empty($_POST)){
		$errors=array();
		if (empty($_POST['username'])) {
			$errors["un"]="Kasutajanimi vajalik";
		}
		if (empty($_POST['passwd'])){
			$errors["pw"]="parool vajalik";
		}
		if (empty($errors)){
			if ($_POST['username']=="test" && $_POST['passwd']=="parool"){
				$_SESSION['user']="test";
				$_SESSION['notice']="sisselogimine edukas";
				header("Location: ?mode=galerii");
				exit(0);
			} else {
				$errors["wr"]="vale info. Proovi uuesti";
			}
		}
	}

	include_once("views/head.html");
	include("views/login.html");
	include_once("views/foot.html");
}

function kuva_register(){
	include_once("views/head.html");
	include("views/register.html");
	include_once("views/foot.html");
}

function kuva_pildivorm(){
	on_sees();
	$errors=array();
	$pilt=array();
	if (!empty($_GET['id'])){ // kui on ok id urlis siis teha massiiv
		$pilt=hangi_pilt( $_GET['id'] ); // kas tühimassiiv või täis massiiv
	}
	// siit alates 13. prax
	/*
	if (!empty($_POST)){
		if (empty($_POST['pealkiri'])) {
			$errors['pk']="pildil peab olema pealkiri";
		} else{
			$pilt['title']=$_POST['pealkiri'];
		}
		if (empty($_POST['autor'])) {
			$errors['au']="pildil peab olema autor";
		} else{
			$pilt['author']=$_POST['autor'];
		}
		if (!empty($_POST['suur'])) {
			$pilt['big']=$_POST['suur'];
		}
		if (!empty($_POST['v2ike'])) {
			$pilt['small']=$_POST['v2ike'];
		}
	}*/
	include_once("views/head.html");
	include("views/pildivorm.html");
	include_once("views/foot.html");
}


function kuva_pealeht(){
	include_once("views/head.html");
	include("views/pealeht.html");
	include_once("views/foot.html");
}

?>