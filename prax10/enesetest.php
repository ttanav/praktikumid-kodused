
<!DOCTYPE>
<html>
<header>
	<meta charset="utf-8" />
	<title>Enesetest 10</title>
	<script
src="http://code.jquery.com/jquery-1.11.0.min.js">
</script>
	<style type="text/css">
	fieldset {
		margin:5px;
	}
.correct {
	color:green;
}
.incorrect {
	color:red;
}
#action {
	position: fixed;
	left:0px;
	right: 0px;
	bottom: 0px;
	padding:10px 100px;
	background: #800080;
	border-top:solid grey 1px;
}
body {
	padding-bottom:50px;
}
input[type="radio"] {
	margin-top: 10px;
}
legend {font-weight: bold;}
	</style>
</header>
<body>
<?php // &lt;&gt;  &#36;

$test=array(
	array("q"=>"Cookie e küpsis on", "a"=>1, "o"=> array("infomassiiv", "string", "number")),
	
	array("q"=>"Küpsis luuakse", "a"=>1, "o"=> array("kasutaja veebilehitsejas", "serveris")),
	
	array("q"=>"Küpsis salvestatakse", "a"=>0, "o"=> array("kasutaja veebilehitsejasse", "serverisse")),
	
	array("q"=>"Küpsis", "a"=>2, "o"=> array("ei liigu pärast loomist enam päringutega kaasa", "saadetakse iga päringuga kaasa", "saadetakse tema loonud serverisse iga päringuga kaasa")),
	
	array("q"=>"Küpsis kehtib", "a"=>2, "o"=> array("temale määratud kehtimisaja lõpuni", "veebilehitseja sulgemiseni", "mõlemad ülevalmainitud vastavalt olukorrale ")),
	
	array("q"=>"Küpsis edastatakse", "a"=>0, "o"=> array("päringu sees päisena", "päringu sees infona (nagu POST meetodil)", "failina")),
	
	array("q"=>"Küpsise loomiseks", "a"=>1, "o"=> array("set_cookie('nimi', 'väätus', 30*60, '/');", "setcookie('nimi', 'väätus', 30*60, '/'); ", "savecookie('nimi', 'väätus', 30*60, '/');")),
	
	array("q"=>"eelnevas küsimuses loodud küpsise väärtuse saab PHP-s kätte", "a"=>2, "o"=> array("järgmisel päringul &#36;COOKIE['nimi'] massiiviväljast", "järgmisel päringul &#36;_Cookie['nimi'] massiiviväljast", "järgmisel päringul &#36;_COOKIE['nimi'] massiiviväljast")),

	array("q"=>"Küpsise kustutamiseks", "a"=>2, "o"=> array("kasutatakse veebilehitsejasse sisseehitatud küpsisehaldurit", "Luuakse uus sama nimega küpsis, mille kehtivusaeg on minevikus", "sobivad kõik ülevalmainitud tegevused")),
		
	array("q"=>"Sessioon luuakse", "a"=>1, "o"=> array("kasutaja arvutis", "serveris")),
	
	array("q"=>"Sessiooniga seoses info salvestatakse", "a"=>0, "o"=> array("serverisse", "kasutaja arvutisse")),
	
	array("q"=>"Uue sessiooni loomise käigus luuakse üldjuhul", "a"=>2, "o"=> array("sessioonivõti, mis salvestatakse failina serverisse", "sessioonifail, mis saadetakse kasutaja arvutisse", "sessioonivõti, mis saadetakse küpsisena kasutaja arvutisse")),
	
	array("q"=>"Sessiooniga seoses olev info tuvastatakse", "a"=>0, "o"=> array("päringus küpsisena saadetud sessioonivõtme põhjal", "päringu teinud masina ip-aadressi põhjal", "päringuga saadetud sessioonifaili põhjal")),
	
	array("q"=>"PHP-s sessiooni alustamiseks kutsutakse välja", "a"=>1, "o"=> array("start_session();  funktsiooni", "session_start(); funktsiooni", "sessionstart(); funktsiooni")),
	
	array("q"=>"eelnevas küsimuses olevat funktsiooni tuleb kutsuda välja", "a"=>0, "o"=> array("üks kord igal programmi käivitamisel, kui selles on vaja sessiooni infot kätte saada", "igal lehe laadimisel", "ainult sessiooni alustamisel")),
	
	array("q"=>"Sessiooni salvestatud info saab PHP-s kätte", "a"=>2, "o"=> array("&#36;SESSION['nimi'] massiivist", "&#36;_Session['nimi'] massiivist", "&#36;_SESSION['nimi'] massiivist")),
	
	array("q"=>"Sessioni lõpetamiseks kutsutakse välja", "a"=>1, "o"=> array("destroy_session(); funktsioon", "session_destroy(); funktsioon", "sessiondestroy(); funktsioon"))
	);
?>
<p>Enesetest on mõeldud selleks, et saaksite kontrollida oma arusaama loengus räägitud teemadel. Tulemusi ei salvestata.<br/>Küsimuste arv: <?php echo count($test); ?></p>
<form action="?" method="POST" id="enesele">
	<?php
		foreach($test as $id => $question){
			echo "<fieldset><legend>{$question['q']}</legend>";
			foreach ($question["o"] as $o_id => $o_text ){
				$extra="";
				$checked="";
				if (isset($_POST['answers']) && isset($_POST['answers'][$id]) && $_POST['answers'][$id]==$o_id){
					$checked="checked";
					if ($question['a']==$o_id) $extra="class=\"correct\"";
					else $extra="class=\"incorrect\"";
				}
				echo "<input type=\"radio\" id=\"a{$id}-{$o_id}\" name=\"answers[$id]\" value=\"{$o_id}\" {$checked} /><label for=\"a{$id}-{$o_id}\" {$extra} >{$o_text}</label><br/>";
			}
			echo "</fieldset>";
		}
	?><br/>

</form>
<p>On ideid rohkemateks küsimusteks? Kirjutage julgelt tiia.tanav[at]itcollege.ee </p>

<div id="action"><input type="submit" value="Testin teadmisi" form="enesele" /></div>
</body>
</html>
