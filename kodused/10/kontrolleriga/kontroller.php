<?php
require_once("vaated/head.html");
session_start();

$pildid = array(
		1=>array('src'=>"pildid/nameless1.jpg", 'alt'=>"nimetu 1"),
		2=>array('src'=>"pildid/nameless2.jpg", 'alt'=>"nimetu 2"),
		3=>array('src'=>"pildid/nameless3.jpg", 'alt'=>"nimetu 3"),
		4=>array('src'=>"pildid/nameless4.jpg", 'alt'=>"nimetu 4"),
		5=>array('src'=>"pildid/nameless5.jpg", 'alt'=>"nimetu 5"),
		6=>array('src'=>"pildid/nameless6.jpg", 'alt'=>"nimetu 6"),
	);
$page="pealeht";
if (isset($_GET['page']) && $_GET['page']!=""){
	$page=htmlspecialchars($_GET['page']);
}

if (isset($_GET['reset'])) {
	session_destroy();
	header("Location: ?page=$page");
	exit(0);
}

switch($page){
	case "galerii":
		include("vaated/galerii.html");
	break;
	case "vote":
		if (isset($_SESSION['pilt']) && isset($pildid[$_SESSION['pilt']])) {
				$id=$_SESSION['pilt'];
			include("vaated/tulemus.html");
		}
		else
		include("vaated/vote.html");
	break;
	case "tulemus":
		$id=false;
		if (isset($_POST['pilt']) && isset($pildid[$_POST['pilt']])){
			$id=htmlspecialchars($_POST['pilt']);
			if (isset($_SESSION['pilt']) && isset($pildid[$_SESSION['pilt']]))
				$id=$_SESSION['pilt'];
			else
				$_SESSION['pilt']=$id;
		}
		include("vaated/tulemus.html");
	break;
	default:
	 include('vaated/pealeht.html');
}


require_once("vaated/foot.html");
?>
