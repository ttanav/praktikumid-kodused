<?php
require_once("head.html");
$pildid = array(
		1=>array('src'=>"pildid/nameless1.jpg", 'alt'=>"nimetu 1"),
		2=>array('src'=>"pildid/nameless2.jpg", 'alt'=>"nimetu 2"),
		3=>array('src'=>"pildid/nameless3.jpg", 'alt'=>"nimetu 3"),
		4=>array('src'=>"pildid/nameless4.jpg", 'alt'=>"nimetu 4"),
		5=>array('src'=>"pildid/nameless5.jpg", 'alt'=>"nimetu 5"),
		6=>array('src'=>"pildid/nameless6.jpg", 'alt'=>"nimetu 6"),
	);
if (isset($_GET['pilt']) && isset($pildid[$_GET['pilt']])):
	$id=htmlspecialchars($_GET['pilt']);
?>
	<h3>Oled teinud oma valiku</h3>
	<p>Valisid selle ilusa pildi: <br/>
		<img src="<?php echo $pildid[$id]['src'];?>" alt="väljavalitu"/></p>
		<a href="vote.php">vali uuesti!</a>
<?php else: ?>
	<h3>Sellist pilti ei eksisteeri, palun tee uus <a href="vote.php">valik</a>.</h3>
<?php
endif;

require_once("foot.html");
?>