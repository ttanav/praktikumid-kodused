var info={
	"1": {"img": "Pictures/sign.jpg", "autor":"Keegi inimene", "pealkiri":"mingi silt on 1"},
	"2": {"img": "Pictures/pilt.png", "autor":"Keegi inimene", "pealkiri":"mingi silt on 2"},
	"3": {"img": "Pictures/sign.jpg", "autor":"Keegi inimene", "pealkiri":"mingi silt on 3"},
	"4": {"img": "Pictures/sign.jpg", "autor":"Keegi inimene", "pealkiri":"mingi silt on 4"},
	"5": {"img": "Pictures/sign.jpg", "autor":"Keegi inimene", "pealkiri":"mingi silt on 5"}
}

window.onload = function() {

galerii=document.getElementById('galerii');
if (typeof(galerii) != 'undefined' && galerii != null) {
	console.log('galerii!');
	
	// lisada sündmus
	lingid=galerii.getElementsByTagName("img");
	for (i=0; i<lingid.length; i++){
		lingid[i].data=i;
		lingid[i].onclick=function(){
			showDetails(this.data);
		}
	}
	// teha suure vaate elemendid
	hoidja=document.createElement('div');
	hoidja.setAttribute("id", "hoidja");
	hoidja.innerHTML="<div id=\"taust\"></div><div id=\"sulge\" onclick=\"hideDetails();\">Sulge</div><div id=\"DetSisu\"><img id=\"suurpilt\" src=\"Pictures/sign.jpg\" alt=\"ajutine\"/><p></p></div>";
	document.body.appendChild(hoidja);


}

}
function showDetails(id){
	id=id+1;
	hoidja=document.getElementById('hoidja');
	if (typeof(hoidja) != 'undefined' && hoidja != null) {
		document.getElementById("suurpilt").src=info[id]['img'];
		hoidja.getElementsByTagName("p")[0].innerHTML="\""+info[id]['pealkiri']+"\" - "+info[id]['autor'];
		hoidja.style.display="initial";
	}

}
function hideDetails() {
	hoidja=document.getElementById('hoidja');
	if (typeof(hoidja) != 'undefined' && hoidja != null) {
		hoidja.style.display="none";
	}
}