<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <script type="text/javascript" src="../jquery-1.8.1.min.js"></script>
 <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script> 
 <title>Praktikum 8 - POST ja GET</title>

  <style type="text/css">
		/*ol li {margin-top:10px;}
		ul li {margin-top:3px;}  
		strong {color: red;}
		span:hover {cursor:pointer;}*/
		li.L0, li.L1, li.L2, li.L3, li.L4, li.L5, li.L6, li.L7, li.L8, li.L9
{
    color: #555;
    list-style-type: decimal;
}
li.L1, li.L3, li.L5, li.L7, li.L9 {
    background: #eee;
}

  </style>
</head>
<body>

<?php
$myurl=$_SERVER['PHP_SELF'];

if (isset($_REQUEST['info'])){
	echo "<p>_REQUEST massiivis on väli 'info': ".$_REQUEST['info']."</p>";
	}

if (isset($_POST['info'])){
	echo "<p>_POST massiivis on väli 'info': ".$_POST['info']."<br/>Pane tähele, et aadressiribal on ainult selle lehe aadress</p>";
	}
	
if (isset($_GET['info'])){
	echo "<p>_GET massiivis on väli 'info': ".$_GET['info']."<br/>Pane tähele, et aadressiribal on GET parameetrid kuvatud</p>";
	}
	
?>
<form action="infoedastus.php" method="GET">
	<input type="text" name="info" value="saadame GET meetodiga"/>
	<input type="submit" value="saada GET!"/>
</form>

<form action="infoedastus.php" method="POST">
	<input type="text" name="info" value="saadame POST meetodiga"/>
	<input type="submit" value="saada POST!"/>
</form>

<p>
	<a href="<?php echo $myurl?>?info=lingis+on+GET" >GET parameetriga link</a>
</p>
<p>lähtekood:</p>
<pre class="prettyprint linenums lang-php">
&lt;?php
// jäädvustame selle lehe aadressi, et saaks kerge vaevaga sellele linkida
$myurl=$_SERVER['PHP_SELF'];

if (isset($_REQUEST['info'])){
	echo "&lt;p>_REQUEST massiivis on väli 'info': ".$_REQUEST['info']."&lt;/p>"; }
	
if (isset($_POST['info'])){
	echo "&lt;p>_POST massiivis on väli 'info': ".$_POST['info']."&lt;br/>Pane tähele, et aadressiribal on ainult selle lehe aadress&lt;/p>"; }
	
if (isset($_GET['info'])){
	echo "&lt;p>_GET massiivis on väli 'info': ".$_GET['info']."&lt;br/>Pane tähele, et aadressiribal on GET parameetrid kuvatud&lt;/p>"; }
?&gt;
&lt;form action="&lt;?php echo $myurl?&gt;" method="GET">
	&lt;input type="text" name="info" value="saadame GET meetodiga"/>
	&lt;input type="submit" value="saada GET!"/>
&lt;/form>

&lt;form action="&lt;?php echo $myurl?&gt;" method="POST">
	&lt;input type="text" name="info" value="saadame POST meetodiga"/>
	&lt;input type="submit" value="saada POST!"/>
&lt;/form>

&lt;p>&lt;a href="&lt;?php echo $myurl?&gt;?info=lingis+on+GET">GET parameetriga link&lt;/a>&lt;/p></pre>

</body>
</html>