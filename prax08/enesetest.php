<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Enesetest 08</title>
	<script
src="http://code.jquery.com/jquery-1.11.0.min.js">
</script>
	<style type="text/css">
	fieldset {
		margin:5px;
	}
.correct {
	color:green;
}
.incorrect {
	color:red;
}
#action {
	position: fixed;
	left:0px;
	right: 0px;
	bottom: 0px;
	padding:10px 100px;
	background: #800080;
	border-top:solid grey 1px;
}
body {
	padding-bottom:50px;
}
input[type="radio"] {
	margin-top: 10px;
}
legend {font-weight: bold;}
	</style>
</head>
<body>
<?php // &lt;&gt;
$test=array(
	array("q"=>"Päringu saadab üldjuhul", "a"=>1, "o"=> array("server kliendile", "klient serverile")),
	
	array("q"=>"vastuse saadab üldjuhul", "a"=>0, "o"=> array("server kliendile", "klient serverile")),
	
	array("q"=>"Päringu meetodiks võib olla", "a"=>5, "o"=> array("GET", "POST", "PUT", "DELETE", "ainult kaks esimest meetodit", "kõik ülevalmainitud meetodid")),
	
	array("q"=>"GET meetodi puudus infoedastusel seisneb selles, et", "a"=>1, "o"=> array("lingi jagamisel/bookmarkimisel on näha (üldjuhul) sama pilt", "kogu sisselogimisinfo on aadressiribal/lingis näha", "lehe värskendamisel näeb (üldjuhul) samat pilti")),
	
	array("q"=>"POST meetodi puudus lehel navigeerimisel seisneb selles, et", "a"=>0, "o"=> array("lingi jagamisel/bookmarkimisel on näha (üldjuhul) erinev pilt (tavaliselt pealeht)", "kogu sisestatud info on päringus peidus")),

	array("q"=>"GET meetodit kasutatakse üldjuhul", "a"=>1, "o"=> array("salvestatava/töödeldava info saatmiseks", "lehe lülitamiseks teise reziimi (vastavalt väärtusele käivitatakse erinev kood)")),

	array("q"=>"POST meetodit kasutatakse üldjuhul", "a"=>0, "o"=> array("salvestatava/töödeldava info saatmiseks", "lehe lülitamiseks teise reziimi (vastavalt väärtusele käivitatakse erinev kood)")),
	
	array("q"=>"PHP-s saab GET meetodil saadetud päringu info kätte", "a"=>0, "o"=> array("&#36;_GET massiivist", "&#36;GET massiivist", "&#36;_Get massiivist")),
	
	array("q"=>"PHP-s saab POST meetodil saadetud päringu info kätte", "a"=>2, "o"=> array("&#36;_Post massiivist", "&#36;POST massiivist", "&#36;_POST massiivist")),
	
	array("q"=>"vastavates massiivides on väljade võtmeteks", "a"=>1, "o"=> array("vormis olnud väljade id atribuutide väärtused", "vormis olnud name atribuutide väärtused", "vormis olnud väljade järjekorranumbrid")),
	
	array("q"=>"vastavates massiivides on väljade väärtusteks", "a"=>2, "o"=> array("vormiväljade value atribuutide vääärtused", "vormiväljadesse sisestatud väärtused", "sõltuvalt olukorralse kas esimene või teine")),

	array("q"=>"Pärast POST meetodil saadud info töötlemist soovitatakse", "a"=>0, "o"=> array("suunata kasutaja mujale, et lehte värskendades infot topelt ei sisestataks", "kuvada kasutajale tulemusi")),

	array("q"=>"Selleks, et saada päringuga seotud infot serverilt kasutatakse", "a"=>1, "o"=> array("&#36;SERVER massiivi", "&#36;_SERVER globaalmassiivi", "&#36;_Server massiivi")),

	array("q"=>"eelnevalt mainitud massiivist saab käivitatud php faili aadressi kätte", "a"=>0, "o"=> array("PHP_SELF väljast", "php_self väljast", "SELF väljast"))
	
	);
?>
<p>Enesetest on mõeldud selleks, et saaksite kontrollida oma arusaama loengus räägitud teemadel. Tulemusi ei salvestata.<br/>Küsimuste arv: <?php echo count($test); ?></p>
<form action="?" method="POST" id="enesele">
	<?php
		foreach($test as $id => $question){
			echo "<fieldset><legend>{$question['q']}</legend>";
			foreach ($question["o"] as $o_id => $o_text ){
				$extra="";
				$checked="";
				if (isset($_POST['answers']) && isset($_POST['answers'][$id]) && $_POST['answers'][$id]==$o_id){
					$checked="checked";
					if ($question['a']==$o_id) $extra="class=\"correct\"";
					else $extra="class=\"incorrect\"";
				}
				echo "<input type=\"radio\" id=\"a{$id}-{$o_id}\" name=\"answers[$id]\" value=\"{$o_id}\" {$checked} /><label for=\"a{$id}-{$o_id}\" {$extra} >{$o_text}</label><br/>";
			}
			echo "</fieldset>";
		}
	?><br/>

</form>
<p>On ideid rohkemateks küsimusteks? Kirjutage julgelt tiia.tanav[at]itcollege.ee </p>

<div id="action"><input type="submit" value="Testin teadmisi" form="enesele" /></div>
</body>
</html>