<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Enesetest 03</title>
	<style type="text/css">
	fieldset {
		margin:5px;
	}
legend {font-weight: bold;}
.correct {
	color:green;
}
.incorrect {
	color:red;
}
#action {
	position: fixed;
	left:0px;
	right: 0px;
	bottom: 0px;
	padding:10px 100px;
	background: #800080;
	border-top:solid grey 1px;
}
body {
	padding-bottom:50px;
}
input[type="radio"] {
	margin-top: 10px;
}
	</style>
</head>
<body>
<?php // &lt;&gt;
$test=array(
	array("q"=>"CSS-i kirjutatakse", "a"=>3, "o"=> array("HTML dokumendi päisesse konkreetse stiili elemendi sisse", ".css lõpulistesse failidesse, mis lingitakse HTML dokumendiga spetsiaalse link elemendi abil", "HTML elementide style atribuudi väärtuseks", "kõikidesse ülalloetletud kohtadesse")),
	
	array("q"=>"CSS failiga lingitud stiili ning HTML dokumendi päises kirjutatud stiili kattumise (sama element, sama omadus, erinev väärtus) korral", "a"=>2, "o"=> array("rakenduvad kõik css omaduse väärtused", "jääb kehtima omaduse kõige esimesena (.css failis) kirjeldatud  väärtus", "jääb kehtima omaduse kõige viimasena (HTML dok. päises) kirjeldatud  väärtus")),
	
	array("q"=>"korrektselt kirjutatud CSS", "a"=>0, "o"=> array("div { background:grey; color:white; }", "div ( background:grey; color:white; )", "&lt;div background:grey; color:white; &gt;")),
	
	array("q"=>"selektoriga <b>a</b> selekteeritakse", "a"=>1, "o"=> array("Lehel esimene lingielement", "Kõik lingielemendid lehel sõltumata nende asukohast", "Kõik lingielemendid, mis pole omakorda ühe teise elemendi sees (on otse body vahel)")),
	
	array("q"=>"selektoriga <b>#arrow</b> selekteeritakse", "a"=>2, "o"=> array("arrow nimelised elemendid (&lt;arrow&gt;&lt;/arrow&gt;)", "kõik elemendid class=\"arrow\" atribuudiga", "element id=\"arrow\" atribuudiga")),
	
	array("q"=>"selektoriga <b>.parem</b> selekteeritakse", "a"=>1, "o"=> array("parem nimelised elemendid (&lt;parem&gt;&lt;/parem&gt;)", "kõik elemendid class=\"parem\" atribuudiga", "element id=\"parem\" atribuudiga")),
	
	array("q"=>"selektoriga <b>img.parem</b> selekteeritakse", "a"=>0, "o"=> array("kõik img elemendid class=\"parem\" atribuudiga", "kõik elemendid class=\"parem\" atribuudiga", "kõik img elemendid, mis on paremasse äärde joomdatud")),
	
	array("q"=>"selektoriga <b>p a</b> selekteeritakse", "a"=>2, "o"=> array("kõik paragrahvid ja lingid", "kõik lingid, mis on otse paragrahvide vahel", "kõik lingid paragrahvide vahel, ükskõik kas lingil on või ei ole veel teisigi elemente ümber")),

	
	array("q"=>"selektoriga <b>p, a</b> selekteeritakse", "a"=>0, "o"=> array("kõik paragrahvid ja lingid", "kõik lingid, mis on otse paragrahvide vahel", "kõik lingid paragrahvide vahel, ükskõik kas lingil on või ei ole veel teisigi elemente ümber")),
	
	array("q"=>"selektoriga <b>div > a</b> selekteeritakse", "a"=>1, "o"=> array("kõik lingi elemendid, mis asuvad otse div elemendi vahel (&lt;div&gt;&lt;a href=\"etc.html\"&gt;link&lt;/a&gt;&lt;/div&gt;)", "kõik lingi elemendid mis asuvad kuskil div elemendi vahel (&lt;div&gt;&lt;a href=\"etc.html\"&gt;link&lt;/a&gt;&lt;/div&gt; ja &lt;div&gt;&lt;p&gt;&lt;a href=\"etc.html\"&gt;link&lt;/a&gt;&lt;/p&gt;&lt;/div&gt;)", "kõik lingi elemendid mis on pärast div elementi  (&lt;div&gt;&lt;/div&gt;&lt;a href=\"etc.html\"&gt;link&lt;/a&gt;)")),
	
	array("q"=>"Css-is kasutatav möötühik on", "a"=>6, "o"=> array("pikslid (px)", "m-tähe laiused (em)", "tollid (in)", "protsendid (%)", "sentimeeter (cm)", "millimeeter (mm)",  "kõik ülalloetletud")),
	
	array("q"=>"omadus <b>font-size</b>", "a"=>0, "o"=> array("määrab teksti suurust", "määrab teksti paksust", "määrab teksti pikkust")),
	
	array("q"=>"elemendi tausta määramisega ei ole seotud omadus", "a"=>3, "o"=> array("background-image", "background-color", "background", "background-width","background-position", "background-repeat", "background-attachment", "background-size")),
	
	array("q"=>"elemendile piirjoone andmiseks", "a"=>1, "o"=> array("tuleb kindlasti määrata kõik - borderi stiil, värv ja laius eraldi omadustena", "tuleb kindlasti määrata piirjoone stiil. Värvi ja laiuse määramine pole kohustuslik.", "tuleb kindlasti määrata kõik - borderi stiil, värv ja laius kas eraldi omadustena või ühendomadusena (border)")),
	
	array("q"=>"värvide esitamiseke on CSS-is kasutusel", "a"=>2, "o"=> array("2 kirjaplti: #NrNrNr (hex värvikood) ja värvinimetus", "3 kirjapilti: #NrNrNr (hex värvikood), rgb(Nr, Nr, Nr) (RGB värvikood) ja värvinimetus", "6 kirjapilti: #NrNrNr (hex värvikood), rgb(Nr, Nr, Nr) (RGB värvikood), rgba(Nr, Nr, Nr, Nr) (RGB värvikood läbipaistvusega), hsl(Nr, protsent, protsent) ( värvus, erksus, heledus värvikood), hsla(Nr, protsent, protsent, Nr) (HSL värvikood läbipaistvusega), ja värvinimetus")),
	
	array("q"=>"fonti saab muuta omadusega", "a"=>2, "o"=> array("font", "font-face", "font-family")),
	
	array("q"=>"omadusega <b>color</b> määratakse", "a"=>1, "o"=> array("teksti tausta värvi", "teksti värvi", "kogu lehe tausta värvi")),
	
	array("q"=>"teksti joondamiseks kasutatakse", "a"=>0, "o"=> array("text-align omadust väärtustega left / right / center / justify", "text-direction omadust väärtustega left / right / center / justify", "text-flow omadust väärtustega left / right / center / justify")),
	
	array("q"=>"omadus <b>text-decoration</b>", "a"=>2, "o"=> array("võimaldab lisada tekstile erinevaid kaunistavaid pildikesi", "võimaldab määrata teksti kuvamist paksult / kaldes / allajoonitult", "võimaldab määrata teksti alla / läbi / ülalt joonimist")),
	
	array("q"=>"teksti kaldes esitamiseks kasutatakse", "a"=>1, "o"=> array("text-style omadust", "font-style omadust", "italic omadust")),
	
	array("q"=>"teksti paksuse määramiseks kasutatakse", "a"=>2, "o"=> array("text-weight omadust", "boldness omadust", "font-weight omadust")),
	
	array("q"=>"CSS stiilikirjeldus<br/>p {text-align:justify; color:orange; border:dotted; }<br/> ütleb, et", "a"=>0, "o"=> array("paragrahvidel on oranž tekst joondatud äärest ääreni ning ümbritsetud 3px laiuse oranži täpilise piirjoonega", "paragrahvidel on oranž tekst joondatud äärest ääreni ning ümbritsetud 3px laiuse musta täpilise piirjoonega", "paragrahvidel on oranž tekst joondatud äärest ääreni ning ümbritsetud 1px laiuse oranži täpilise piirjoonega"))


	);
?>
<p>Enesetest on mõeldud selleks, et saaksite kontrollida oma arusaama loengus räägitud teemadel. Tulemusi ei salvestata.<br/>Küsimuste arv: <?php echo count($test); ?></p>
<form action="?" method="POST" id="enesele">
	<?php
		foreach($test as $id => $question){
			echo "<fieldset><legend>{$question['q']}</legend>";
			foreach ($question["o"] as $o_id => $o_text ){
				$extra="";
				$checked="";
				if (isset($_POST['answers']) && isset($_POST['answers'][$id]) && $_POST['answers'][$id]==$o_id){
					$checked="checked";
					if ($question['a']==$o_id) $extra="class=\"correct\"";
					else $extra="class=\"incorrect\"";
				}
				echo "<input type=\"radio\" id=\"a{$id}-{$o_id}\" name=\"answers[$id]\" value=\"{$o_id}\" {$checked} /><label for=\"a{$id}-{$o_id}\" {$extra} >{$o_text}</label><br/>";
			}
			echo "</fieldset>";
		}
	?><br/>

</form>
<p>On ideid rohkemateks küsimusteks? Kirjutage julgelt tiia.tanav[at]itcollege.ee </p>

<div id="action"><input type="submit" value="Testin teadmisi" form="enesele" /></div>
</body>
</html>
