<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Enesetest 05</title>
	<style type="text/css">
	fieldset {
		margin:5px;
	}
.correct {
	color:green;
}
.incorrect {
	color:red;
}
#action {
	position: fixed;
	left:0px;
	right: 0px;
	bottom: 0px;
	padding:10px 100px;
	background: #800080;
	border-top:solid grey 1px;
}
body {
	padding-bottom:50px;
}
input[type="radio"] {
	margin-top: 10px;
}
legend {font-weight: bold;}
	</style>
</head>
<body>
<?php // &lt;&gt;
$test=array(
	array("q"=>"JavaScripti kirjutatakse", "a"=>3, "o"=> array("sündmusartibuutide väärtusesse", ".js -lõpulistesse failidesse, mis seotakse HTML falidega", "&lt;script type=\"text/javascript\"&gt; ja &lt/script&gt; vahele kas dokumendi päises või kehas", "kõiki eelnevalt mainitud kohtadesse")),
	
	array("q"=>"korrektne viide javascripti failile", "a"=>0, "o"=> array("&lt;script src=\"fail.js\" type=\"text/javascript\"&gt;&lt;/script&gt;", "&lt;script type=\"text/javascript\"&gt;fail.js&lt;/script&gt;", "&lt;script type=\"text/javascript\"&gt;import(fail.js)&lt;/script&gt;")),
	
	array("q"=>"Javascripti koodis kasutatakse kommenteerimiseks ", "a"=>3, "o"=> array("=begin <br/> kommentaar <br/>=end", "&lt;!-- kommentaar --&gt;", "# kommentaar", "// kommentaar <br/> ja /* pikem kommentaar */")),
	
	array("q"=>"semikoolonid JS käskude lõpus", "a"=>0, "o"=> array("kui käsud on eri ridadel siis ei ole kohustuslikud", "ei ole kohustuslikud", "on kohustuslikud")),
	
	array("q"=>"funktsiooni loomiseks", "a"=>2, "o"=> array("nimi (parameetrid) { fn blokk }", "funktsioon nimi(parameetrid) { fn blokk }", "function nimi(parameetrid){ fn blokk }")),
	
	array("q"=>"JavaScripti käivitamisel võib tekkida probleeme, kui", "a"=>1, "o"=> array("Javascript loetakse/laetakse pärast HTMl faili laadimise lõppu", "mõjutatavad elemendid pole lehel JS koodi käivitamise hetkeks ära laetud", "mõjutatavate elementide objekte ei salvestata muutujatesse")),
	
	array("q"=>"lehe laadimise lõppu tähistav sündmus on ", "a"=>1, "o"=> array("window.load", "window.onload", "document.ready")),
	
	array("q"=>"<pre>var a=\"tere\";\nfunction asd(){\n  var a=\"tali\"; \n} \nasd();\nalert(a);</pre> <br/>väljastab", "a"=>0, "o"=> array("tere", "tali", "tere tali")),
	
	array("q"=>"<pre>var a=\"tere\";\nfunction asd(){\n  a=\"tali\"; \n} \nalert(a);</pre>  <br/>väljastab", "a"=>0, "o"=> array("tere", "tali", "tere tali")),
	
	array("q"=>"Javascripti muutujad", "a"=>1, "o"=> array("on tüübikindlad - muutuja väärtuse muutmisel peab uus väärtus olema samat tüüpi (täisarvu sisaldanud muutuja väärtus saab olla ainult täisarv)", "pole tüübikindlad - ükskõik millist tüüpi muutujat võib üle kirjutada ükskõik mis tüüpi väärtusega (täisarvu asemel võib muutuja väärtustada nt stringiga)")),
	
	array("q"=>"massiivi kirjeldamiseks", "a"=>4, "o"=> array("kalad = new Array('haug', 'kilu', 'lest');", "kalad = ['haug', 'kilu', 'lest']", "kalad = new Array(); kalad.push('haug'); kalad.push('kilu'); kalad.push('lest'); ", "kalad = new Array(); kalad[0]='haug'; kalad[1]='kilu'; kalad[2]='lest'; ", "Kõik eelnevad", "ainult esimene", "ainult esimesed kaks")),
	
	array("q"=>"Massiivis tallid, kus on 6 elementi saab viimase elemendi kätte", "a"=>2, "o"=> array("tallid.last", "tallid[tallid.length]", "tallid[tallid.length-1]")),
	
	array("q"=>"id järgi elemendi selekteerimiseks kasutatakse JS-is", "a"=>0, "o"=> array("document.getElementById('id')", "document.getElementsById('id')", "document.getElementById('#id')")),
	
	array("q"=>"document.getElementsByName('nimi') kasutamisel tagastatakse", "a"=>2, "o"=> array("Esimene dokumendis leiduv 'nimi' nimeline HTML element ", "Viimane dokumendis leiduv 'nimi' nimeline HTML element ", "'nimi' nimeliste HTML elementide objektidest koosnev massiiv")),
	
	array("q"=>"Klassi järgi elementide selekteerimiseks kasutatakse JS-is", "a"=>1, "o"=> array("document.getElementsByClassName('.klassinimi')", "document.getElementsByClassName('klassinimi')", "document.getElementByClassName('klassinimi')")),
	
	array("q"=>"olgu meil paragrahvi element<pre>el = document.getElementById('kala');</pre> Selleks, et selle stiili (CSS) JS abil muuta kasutatakse", "a"=>2, "o"=> array("el.CSSomadus='uusVäärtus'", "el.css.CSSomadus='uusVäärtus'", "el.style.CSSomadus='uusVäärtus'")),
	
	array("q"=>"JavaScriptis on CSS omaduse nimi kirjutamisel", "a"=>0, "o"=> array("sidekriipsudeta kokku kirjutatud, esimene täht väike, eri sõnade algustähed suured", "täpselt nii nagu CSS-is", "läbivalt väikese tähega kokku kirjutatud ")),
	
	array("q"=>"elemendi atribuutide lisamiseks ja/või muutmiseks", "a"=>2, "o"=> array("el.attr.atribuut='uusVäärtus'", "el.set('atribuut', 'uusVäärtus')", "el.atribuut='uusVäärtus'")),
	
	array("q"=>"elemendi sisu muutmiseks", "a"=>1, "o"=> array("el.html('uus HTML');", "el.innerHTML='uus HTML';", "el.HTML='uus HTML';")),
	
	array("q"=>"sündmusatribuudi väärtuse (sündmuse korral käivitatav kood) määramiseks ", "a"=>2, "o"=> array("kasutatakse<pre>&lt;p onclick=\"/* mida teha */\"&gt;tekst&lt;/p&gt;</pre>kirjapilti", "kasutatakse <pre>el.onclick=function(){\n  /* mida teha */\n}</pre>kirjapilti", "kasutatakse mõlemat ülemist kirjapilti")),
	
	array("q"=>"sündmuse kuulaja kirjeledamiseks kasutatakse", "a"=>0, "o"=> array("el.addEventListener('click', function(){ /*mida teha*/ },false );", "el.addEventListener('onclick', function(){ /*mida teha*/ },false );", "el.onclick= new EventListener( function(){ /*mida teha*/ },false );")),
	


	);
?>
<p>Enesetest on mõeldud selleks, et saaksite kontrollida oma arusaama loengus räägitud teemadel. Tulemusi ei salvestata.<br/>Küsimuste arv: <?php echo count($test); ?></p>
<form action="?" method="POST" id="enesele">
	<?php
		foreach($test as $id => $question){
			echo "<fieldset><legend>{$question['q']}</legend>";
			foreach ($question["o"] as $o_id => $o_text ){
				$extra="";
				$checked="";
				if (isset($_POST['answers']) && isset($_POST['answers'][$id]) && $_POST['answers'][$id]==$o_id){
					$checked="checked";
					if ($question['a']==$o_id) $extra="class=\"correct\"";
					else $extra="class=\"incorrect\"";
				}
				echo "<input type=\"radio\" id=\"a{$id}-{$o_id}\" name=\"answers[$id]\" value=\"{$o_id}\" {$checked} /><label for=\"a{$id}-{$o_id}\" {$extra} >{$o_text}</label><br/>";
			}
			echo "</fieldset>";
		}
	?><br/>

</form>
<p>On ideid rohkemateks küsimusteks? Kirjutage julgelt tiia.tanav[at]itcollege.ee </p>

<div id="action"><input type="submit" value="Testin teadmisi" form="enesele" /></div>
</body>
</html>