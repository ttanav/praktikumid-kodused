<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Enesetest 07</title>
	<script
src="http://code.jquery.com/jquery-1.11.0.min.js">
</script>
	<style type="text/css">
	fieldset {
		margin:5px;
	}
.correct {
	color:green;
}
.incorrect {
	color:red;
}
#action {
	position: fixed;
	left:0px;
	right: 0px;
	bottom: 0px;
	padding:10px 100px;
	background: #800080;
	border-top:solid grey 1px;
}
body {
	padding-bottom:50px;
}
input[type="radio"] {
	margin-top: 10px;
}
label {display: block;}
legend {font-weight: bold;}
pre{ margin: 0px;}
	</style>
</head>
<body>
<?php // &lt;&gt;
$test=array(
	array("q"=>"PHP programmid", "a"=>1, "o"=> array("käivitatakse kasutaja veebilehitsejas, kus kuvatakse selle tulemusena tekkinud HTML-i", "käivitatakse serveris, selle käigus tekkinud HTML edastatakse kasutaja veebilehitsejale")),
	
	array("q"=>"PHP programmid", "a"=>1, "o"=> array("kompileeritakse", "interpreteeritakse")),
	
	array("q"=>"PHP programmi käivitamiseks", "a"=>2, "o"=> array("tuleb ta oma arenduskeskkonnas veebilehitsejaga avada", "tuleb ta laadida suvalisse serverisse ning ta sealt avada", "tuleb ta laadida serverisse, kus on paigaldatud spetsiifiline PHP moodul, ning ta sealt avada")),
	
	array("q"=>"PHP kood käivitatakse kui", "a"=>0, "o"=> array("see on kirjutatud või kaasatud .php lõpulisse faili", "see on kirjutatud või kaasatud .html lõpulisse faili", "see on kirjutatud või kaasatud .txt lõpulisse faili")),
	
	array("q"=>"PHP kood kirjutatakse", "a"=>1, "o"=> array("spetsiaalsete &lt;%= ja %> tagide vahele", "spetsiaalsete PHP tagide &lt;?php ja ?> vahele", "spetsiaalsete &lt;php> ja &lt;/php> tagide vahele")),
	
	array("q"=>"semikoolon ;", "a"=>0, "o"=> array("on PHP käskude lõpus kohustulik, selle puudumisel lõpetab programm töö veateatega", "pole PHP süntaksi osa", "pole PHP käskude lõpus kohustuslik juhul kui käsud on eri ridadel ")),
	
	array("q"=>"Kasutajale millegi ekraanile esitamiseks on vaja see printida. Selleks kasutatakse", "a"=>2, "o"=> array("document.println('tekst'); kirjapilti", "puts 'tekst'; kirjapilti", "echo 'tekst'; kirjapilti")),
	
	array("q"=>"muutujanimede ette", "a"=>1, "o"=> array("ei pea midagi kirjutama (nagu JS-is)", "kirjutatakse alati &#36;-märk", "kirjutatakse &#36;-märk ainult siis kui on tegemist globaalmuutujaga")),
	
	array("q"=>"teksti piiritlemiseks ", "a"=>2, "o"=> array("kasutatakse ülakomasid ' ", "kasutatakse jutumärke \"", "kasutatakse mõlemat ülevalmainitut")),
	
	array("q"=>"stringide (teksti) liitmiseks kasutatakse PHP-s", "a"=>0, "o"=> array("punkti . ", "plussi + ", "spetsiaalset concat('tekst', ' teinetekst'); funktisooni")),
	
	array("q"=>"Kommentaaride kirjutamiseks on PHP-s", "a"=>2, "o"=> array("&lt;%# kommentaar %> kirjapilt", "sama kirjapilt nagu HTML-is (&lt;!-- kommentaar -->)", "sama kirjapilt nagu JS-is (// üherealine ja /* pikem kommentaar */)")),

	array("q"=>"funktsiooni loomiseks", "a"=>2, "o"=> array("kirjutatakse<pre>def nimi (parameetrid)\n  fn blokk\nend</pre>", "kirjutatakse <pre>funktsioon nimi(parameetrid) {\n fn blokk \n}</pre>", "Kirjutatakse<pre>function nimi(parameetrid){ \n	fn blokk \n}</pre>")),
	
	array("q"=>"funktsioonist mingi väärtuse tagastamiseks kirjutatakse", "a"=>1, "o"=> array("ret &#36;muutuja;", "return &#36;muutja;", "^ &#36;muutuja")),
	
	array("q"=>"PHP funktsiooni väljakutsumiseks kirjutatakse", "a"=>2, "o"=> array("funktsiooniNimi &#36;sisendV22rtus, &#36;teineSisend;", "function funktsiooniNimi(&#36;sisendV22rtus, &#36;teineSisend);", "funktsiooniNimi(&#36;sisendV22rtus, &#36;teineSisend);")),
	
	array("q"=>"PHP muutuja loomisel", "a"=>1, "o"=> array("tuleb kirjeldada selle tüüpi (int, string etc.)", "ei ole vaja tüüpi kirjeldada")),
	
	array("q"=>"PHP muutuja mingi väärtusega ülekirjutamisel", "a"=>2, "o"=> array("tuleb muutuja kõigepealt ära kustutada (unset(&#36;muutuja);)", "peab uus väärtus olema samat andmetüüpi (int, string etc.) - PHP muutujad on tüübikindlad", "võib uus väärtus olla ükskõik mis andmetüüpi (int, string etc.) - PHP muutujad ei ole tüübikindlad")),
	
	array("q"=>"kontrollimaks, kas mingi muutja eksisteerib, kasutatakse", "a"=>0, "o"=> array("isset(&#36;muutuja); funktsiooni", "is_set(&#36;muutuja); funktsiooni", "typeof(&#36;muutuja); funktsiooni (kui tüüpi pole, pole ka muutujat)")),
	
	array("q"=>"globaalskoobis (väljaspool funktsioone kirjeldatud) olevad muutujad", "a"=>1, "o"=> array("on funktsioonide sees vabalt kättesaadavad", "tuleb funktsioonide sees kasutamiseks global käsuga registreerida", "tuleb alati sööta funktsiooni sisse selle parameetritena")),
	
	array("q"=>"Korrektne viis mingi tingimuse kehtimisel HTML koodi tekitada on", "a"=>2, "o"=> array("kirjapildiga<pre>&lt;?php if(&#36;muutuja==\"midagi\"){
  echo '&lt;p>see kood väljastab '.&#36;muutuja.' teksti&lt;/p>';
} ?></pre>", "kirjapildiga<pre>&lt;?php if(&#36;muutuja==\"midagi\"):?>
  &lt;p>see kood väljastab &lt;?php echo &#36;muutuja; ?> teksti&lt;/p>
&lt;php endif; ?></pre>", "mõlemad ülevalmainitud")),
	
	array("q"=>"switch lause korrektne kirjapilt", "a"=>0, "o"=> array("on <pre>switch(&#36;muutuja){
  case 'tere': sayhello(); break;
  case 'hyvasti': saybye(); break;
  default: donothing(); break;
}</pre>", "on <pre>switch(&#36;muutuja){
  case 'tere': sayhello(); 
  case 'hyvasti': saybye(); 
  default: donothing(); 		
}</pre>", "on <pre>switch(&#36;muutuja){
  case 'tere': { sayhello(); }
  case 'hyvasti': { saybye(); }
  default: { donothing(); }		
}</pre>")),
	
	array("q"=>"massiivis olevate elementide väärtused", "a"=>0, "o"=> array("võivad olla läbisegi erinevatest andmetüüpidest", "peavad kõik olema samat andmetüüpi")),
	
	array("q"=>"massiivi võtmeteks", "a"=>2, "o"=> array("on alati täisarvud (alates 0)", "on alati stringid", "võivad olla läbisegi nii täisarvud kui ka stringid")),
	
	array("q"=>"Massiivi loomiseks", "a"=>3, "o"=> array("&#36;mass = array();", "&#36;mass = array('abc','def', 1, 2);", "&#36;mass = array('nimi'=>'mustikas', 'varvus'=>'lilla');", 'sobivad kõik ülevalmainitud')),
	
	array("q"=>"Massiivist kolmanda elemendi lugemiseks kirjutatakse", "a"=>0, "o"=> array("&#36;massiiv[2]", "&#36;massiiv[3]", "&#36;massiiv.get(3);")),
		
	array("q"=>"PHP massiivi", "a"=>0, "o"=> array("saab jooksvalt elementidega täiendada", "ei saa pärast selle loomist enam muuta")),
	
	array("q"=>"muutujate mälust eemaldamiseks/kustutamiseks kasutatakse", "a"=>1, "o"=> array("undefine(&#36;muutuja); funktisooni", "unset(&#36;muutuja); funktsiooni", "erase(&#36;muutuja); funktisooni")),
	
	array("q"=>"üle kõigi massiivis olevate elementide itereerimiseks", "a"=>4, "o"=> array("kasutatakse kirjapilti<pre>for(&#36;i; &#36;i<&#36;count(massiiv);&#36;i++){
   //tegevus
}</pre>", "kasutatakse kirjapilti<pre>foreach(&#36;massiiv as &#36;v22rtus){
   //tegevus
}</pre>", "kasutatakse kirjapilti<pre>foreach(&#36;massiiv as &#36;voti => &#36;v22rtus){
   //tegevus
}</pre>", "kasutatakse kirjapilti<pre>&#36;massiiv.each(&#36;voti, &#36;v22rtus){
	//tegevus
}</pre>", "kõiki kirjapilte, väljaarvatud viimast", "kõiki kirjapilte")),
	
	array("q"=>"include ja require vahe seisneb", "a"=>0, "o"=> array("selles, et kui require fail puudub lõpetab PHP programm töö koos veateatega, aga include faili puudumisel jätkab hoiatusega", "selles, et kui include fail puudub lõpetab PHP programm töö koos veateatega, aga require faili puudumisel jätkab hoiatusega", "ainult funktsioonide nimedes"))

	);
?>
<p>Enesetest on mõeldud selleks, et saaksite kontrollida oma arusaama loengus räägitud teemadel. Tulemusi ei salvestata.<br/>Küsimuste arv: <?php echo count($test); ?></p>
<form action="?" method="POST" id="enesele">
	<?php
		foreach($test as $id => $question){
			echo "<fieldset><legend>{$question['q']}</legend>";
			foreach ($question["o"] as $o_id => $o_text ){
				$extra="";
				$checked="";
				if (isset($_POST['answers']) && isset($_POST['answers'][$id]) && $_POST['answers'][$id]==$o_id){
					$checked="checked";
					if ($question['a']==$o_id) $extra="class=\"correct\"";
					else $extra="class=\"incorrect\"";
				}
				echo "<label {$extra} ><input type=\"radio\" name=\"answers[$id]\" value=\"{$o_id}\" {$checked} />{$o_text}</label>";
			}
			echo "</fieldset>";
		}
	?><br/>

</form>
<p>On ideid rohkemateks küsimusteks? Kirjutage julgelt tiia.tanav[at]itcollege.ee </p>

<div id="action"><input type="submit" value="Testin teadmisi" form="enesele" /></div>
</body>
</html>