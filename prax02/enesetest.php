<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Enesetest 02</title>
	<style type="text/css">
	fieldset {
		margin:5px;
	}
legend {font-weight: bold;}
.correct {
	color:green;
}
.incorrect {
	color:red;
}
#action {
	position: fixed;
	left:0px;
	right: 0px;
	bottom: 0px;
	padding:10px 100px;
	background: #800080;
	border-top:solid grey 1px;
}
body {
	padding-bottom:50px;
}
input[type="radio"] {
	margin-top: 10px;
}
	</style>
</head>
<body>
<?php // &lt;&gt;
$test=array(
	array("q"=>"&lt;table&gt; elemendi sisse võib vahetult paigutada", "a"=>2, "o"=> array("Kõiki teisi HTML elemente", "Ainult inline tüüpi elemente", "Ainult tabeliga seotud elemente nagu &lt;caption&gt;, &lt;tr&gt;, &lt;thead&gt;, &lt;tbody&gt;, &lt;tfoot&gt;")),
	
	array("q"=>"&lt;td&gt;, &lt;caption&gt; ja &lt;th&gt; elementide sisse", "a"=>1, "o"=> array("võib lisada ainult teksti", "võib lisada teksti ja suvalisi teisi HTML elemente", "võib lisada ainult tabeliga seotud elemente")),

	array("q"=>"colspan atribuut", "a"=>0, "o"=> array("lisatakse &lt;td&gt; ja &lt;th&gt; elementidele, et nad oleks horisonaalsuunas mitme välja laiused", "lisatakse &lt;tr&gt; elementidele, et nede sees olevad tabeliväljad oleks mitme välja laiused", "lisatakse &lt;table&gt; elemendile, et määrata tabeli piirjoonte vahelist ruumi")),
	
	array("q"=>"colspan=\"3\" atribuudi kasutamisel", "a"=>1, "o"=> array("pole vaja teha muudatusi tabeli struktuuris", "peab vastavas reas olema vastavalt colspan väärtusele 2 välja vähem (&lt;td&gt; või &lt;th&gt; elemente)", "ei tohi selles reas olla ühtegi teist välja")),

	array("q"=>"rowspan atribuut", "a"=>2, "o"=> array("lisatakse &lt;tr&gt; elementidele, et nede sees olevad tabeliväljad oleks mitme välja laiused", "lisatakse &lt;table&gt; elemendile, et määrata tabeli piirjoonte vahelist ruumi", "lisatakse &lt;td&gt; ja &lt;th&gt; elementidele, et nad oleks vertikaalsuunas mitme välja kõrgused")),
	
	array("q"=>"rowspan=\"3\" atribuudi kasutamisel", "a"=>0, "o"=> array("peab järgmises kahes reas vastavas veerus olema 1 väli vähem (&lt;td&gt; või &lt;th&gt; elemente)", "pole vaja teha muudatusi tabeli struktuuris", "ei tohi selles reas olla ühtegi teist välja")),
	
	array("q"=>"Selleks, et tabelil kuvataks ka piirjooned", "a"=>1, "o"=> array("tuleb kindlasti kasutada CSS-i", "tuleb &lt;table&gt; elemendile lisada border=\"1\" atribuut", "tuleb kõigile &lt;td&gt; ja &lt;th&gt; elementidele lisada border=\"1\" atribuut")),
	
	array("q"=>"korrektse tabeli loomisel", "a"=>2, "o"=> array("ei ole tähtis, kuidas ridu ja veerge lisatakse", "peab igas reas olema sama arv välju, kusjuures colspan ja rowspan atribuudiga elemente loetakse üheks väljaks", "peab igas reas olema sama arv välju, kusjuures colspan ja rowspan atribuudiga elemente loetakse mitme väljana (vastavalt atribuudi väärtusele)")),
	
	array("q"=>"Veebivorme kasutatakse", "a"=>1, "o"=> array("kasutaja tüütamiseks", "andmete saatmiseks serverile", "andmete saatmiseks kliendile (kasutaja arvuti)")),
	
	array("q"=>"Valida mittetoimiv vorm", "a"=>2, "o"=> array("&lt;form action=\"mingi.html\" method=\"POST\"&gt;<br/>&lt;input type=\"submit\" /&gt;<br/>&lt;/form&gt;", "&lt;form action=\"mingi.html\" method=\"POST\" id=\"main\" &gt;&lt;/form&gt;<br/>&lt;input type=\"submit\" form=\"main\"/&gt;", "&lt;form action=\"mingi.html\" method=\"GET\" &gt;&lt;/form&gt;<br/>&lt;input type=\"submit\"/&gt;")),
	
	array("q"=>"eelnevast küsimusest järeldan et", "a"=>0, "o"=> array("vormielemendid nagu input asuvad kas &lt;form&gt; elemendi vahel, või on selle id-ga seotud läbi form atribuudi", "vormide puhul peab meetodiks olema alati POST", "vormielemendid võivad asuda kus iganes")),
	
	array("q"=>"input elemendi type atribuudi väärtuseks on", "a"=>11, "o"=> array("submit", "text", "number", "email", "radio", "checkbox", "range", "color", "date", "password", "url", "kõik loetletud")),
	
	array("q"=>"korrektselt kirja pandud &lt;form&gt; elemendi atribuut action", "a"=>1, "o"=> array("sisaldab alati seda vormi ennast sisaldava faili aadressi", "peab sisaldama aadressi, kus saadetavat infot (potentsiaalselt) töödeldakse", "võib jääda tühjaks")),
	
	array("q"=>"vormielementidele nagu  &lt;input&gt;,  &lt;textarea&gt; ja  &lt;select&gt; peab andma name atribuudi kuna", "a"=>2, "o"=> array("muidu on arendajal nendel HTML elementidel raske vahet teha", "muidu veebilehitseja neid ei kuvaks", "muidu ei saadeta vastava välja väärtust vormi esitamisel päringuga kaasa")),
	
	array("q"=>"Selleks, et lehe laadimisel oleks radiobutton või checkbox vaikimisi 'valitud' antakse tema elemendile", "a"=>2, "o"=> array("selected atribuut", "ticked atribuut", "checked atribuut")),
	
	array("q"=>"Radiobuttoni ja checkboxi erinevus seisneb", "a"=>3, "o"=> array("selles, et sama name atribuudi väärtusega radiobuttonite seast saab olla valitud vaid üks, aga checkboxide puhul sellist piirangut pole", "selles, et kord valitud radiobuttoni inaktiivseks muutmiseks on vaja valida sama (nime) grupi teine radiobutton aga checkboxi puhul lihtsalt temale uuesti klikkida", "type atribuudi väärtuses", "Kõikides ülalloetletud faktides")),
	
	array("q"=>"submit tüüpi input elemendil (submit nupul) määrab pealekirjutava teksti", "a"=>1, "o"=> array("name atribuudi väärtus", "value atribuudi väärtus", "placeholder atribuudi väärtus")),
	
	array("q"=>"Textarea elemendi puhul kirjutatakse", "a"=>0, "o"=> array("vaikimisi väärtus elemendi algus- ja lõputagi vahele, prompt placeholder atribuuti", "vaikimisi väärtus value atribuuti ja prompt elemendi algus- ja lõputagi vahele", "vaikimisi väärtus placeholder atribuuti")),
	
	array("q"=>"textarea elemendi tagide vahele kirjutatud teksti puhul", "a"=>1, "o"=> array("kuvatakse tühikud-reavahetused ühe tühikuna, nagu igal pool mujal HTML dokumendis", "kuvatakse kõik märgid (tühikud, tab-id, reavahetused) täpselt selliselt nagu nad on koodis kirjutatud", "tuleb noolsulud jm. HTML-is tähendusega märgid esitada erikujul &amp;nimi;")),
	
	array("q"=>"Select elemendi puhul saadetakse vormi esitamisel", "a"=>2, "o"=> array("select elemendi algustagis asuva name atribuudi ja value atribuudi väärtuspaar", "select elemendi algustagi value atribuudi ja valitud option tagi value atribuudi paar", "select elemendi algustagi name atribuudi ning valitud option elemendi tagide vahelise väärtuse paar value atribuudi puudumisel ning select elemendi algustagi name atribuudi ning valitud option elemendi algustagi value atribuudi väärtuspaar viimase olemasolul.")),
	
	array("q"=>"Selleks, et lehe laadimisel oleks mingi option select elemendis vaikimisi 'valitud' antakse konkreetsele option elemendile", "a"=>0, "o"=> array("selected atribuut", "ticked atribuut", "checked atribuut")),
	
	array("q"=>"option elemendi value atribuudi väärtus ja elemendi tagidevaheline tekst", "a"=>1, "o"=> array( "peavad olema samad", "ei pea olema samad")),
	
	array("q"=>"vormiväljaga seotud label element", "a"=>0, "o"=> array("suurendab selle vormivälja selekteeritavat ala (nt radiobuttonid, checkboxid)", "on ainult dekoratiivne lisand väljendamaks vormivälja eesmärki")),
	
	array("q"=>"div element on vaikimisi", "a"=>2, "o"=> array("tüübitu", "inline tüüpi", "block tüüpi"))


	);
?>
<p>Enesetest on mõeldud selleks, et saaksite kontrollida oma arusaama loengus räägitud teemadel. Tulemusi ei salvestata.<br/>Küsimuste arv: <?php echo count($test); ?></p>
<form action="?" method="POST" id="enesele">
	<?php
		foreach($test as $id => $question){
			echo "<fieldset><legend>{$question['q']}</legend>";
			foreach ($question["o"] as $o_id => $o_text ){
				$extra="";
				$checked="";
				if (isset($_POST['answers']) && isset($_POST['answers'][$id]) && $_POST['answers'][$id]==$o_id){
					$checked="checked";
					if ($question['a']==$o_id) $extra="class=\"correct\"";
					else $extra="class=\"incorrect\"";
				}
				echo "<input type=\"radio\" id=\"a{$id}-{$o_id}\" name=\"answers[$id]\" value=\"{$o_id}\" {$checked} /><label for=\"a{$id}-{$o_id}\" {$extra} >{$o_text}</label><br/>";
			}
			echo "</fieldset>";
		}
	?><br/>

</form>
<p>On ideid rohkemateks küsimusteks? Kirjutage julgelt tiia.tanav[at]itcollege.ee </p>

<div id="action"><input type="submit" value="Testin teadmisi" form="enesele" /></div>
</body>
</html>
