<?php


function kuva_galerii(){
	global $pildid;
	include_once("views/head.html");
	include("views/galerii.html");
	include_once("views/foot.html");
}

function kuva_pilt(){
	global $pildid;
	$id=0;
		if (!empty($_GET['id']) && is_numeric($_GET['id']) && isset($pildid[$_GET['id']]) ){
			$id=$_GET['id'];
		}
		$eelmine=$id-1;
		if ($eelmine<0) $eelmine=count($pildid)-1;
		$jargmine = $id+1;
		if ($jargmine==count($pildid)) $jargmine=0;
		$pilt=$pildid[$id];
		include("views/pilt.html");
}

function kuva_login(){
	if (!empty($_POST)){
		$errors=array();
		if (empty($_POST['username'])) {
			$errors["un"]="Kasutajanimi vajalik";
		}
		if (empty($_POST['passwd'])){
			$errors["pw"]="parool vajalik";
		}
		if (empty($errors)){
			if ($_POST['username']=="test" && $_POST['passwd']=="parool"){
				header("Location: ?mode=galerii");
			} else {
				$errors["wr"]="vale info. Proovi uuesti";
			}
		}
	}

	include_once("views/head.html");
	include("views/login.html");
	include_once("views/foot.html");
}

function kuva_register(){
	include_once("views/head.html");
	include("views/register.html");
	include_once("views/foot.html");
}

function kuva_pildivorm(){
	include_once("views/head.html");
	include("views/pildivorm.html");
	include_once("views/foot.html");
}


function kuva_pealeht(){
	include_once("views/head.html");
	include("views/pealeht.html");
	include_once("views/foot.html");
}

?>